<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translateUri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'common';
$route['404_override'] = '';
$route['translateUri_dashes'] = FALSE;
$route['test'] = 'common/test';

#operator
$route['operator/login']['POST']          = 'operator/login';
$route['operator/refresh']['GET']         = 'operator/refresh';
$route['operator/change-password']['PUT'] = 'operator/change_password';
$route['operator']['GET']                 = 'operator/get_operator';
$route['operator/by-id/(:any)']['GET']    = 'operator/get_operator_by_id/$1';
$route['operator/profile']['GET']         = 'operator/get_profile';
$route['operator']['POST']                = 'operator/create_operator';
$route['operator']['PUT']                 = 'operator/update_operator';
$route['operator/delete/(:any)']['DELETE']= 'operator/delete_operator/$1';

#kategori
$route['kategori']['GET']               = 'kategori/get_kategori';
$route['kategori/all']['GET']           = 'kategori/get_kategori_all';
$route['kategori/by-id/(:any)']['GET']  = 'kategori/get_kategori_by_id/$1';
$route['kategori']['POST']              = 'kategori/create_kategori';
$route['kategori']['PUT']               = 'kategori/update_kategori';
$route['kategori/(:any)']['DELETE']     = 'kategori/delete_kategori/$1';

#Lokasi
$route['lokasi']['GET']               = 'lokasi/get_lokasi';
$route['lokasi/all']['GET']           = 'lokasi/get_lokasi_all';
$route['lokasi/list']['GET']          = 'lokasi/get_lokasi_list';
$route['lokasi/by-id/(:any)']['GET']  = 'lokasi/get_lokasi_by_id/$1';
$route['lokasi']['POST']              = 'lokasi/create_lokasi';
$route['lokasi']['PUT']               = 'lokasi/update_lokasi';
$route['lokasi/(:any)']['DELETE']     = 'lokasi/delete_lokasi/$1';

#suplier
$route['suplier']['GET']               = 'suplier/get_suplier';
$route['suplier/all']['GET']           = 'suplier/get_suplier_all';
$route['suplier/by-id/(:any)']['GET']  = 'suplier/get_suplier_by_id/$1';
$route['suplier']['POST']              = 'suplier/create_suplier';
$route['suplier']['PUT']               = 'suplier/update_suplier';
$route['suplier/(:any)']['DELETE']     = 'suplier/delete_suplier/$1';

#unit
$route['unit']['GET']               = 'unit/get_unit';
$route['unit/all']['GET']           = 'unit/get_unit_all';
$route['unit/by-id/(:any)']['GET']  = 'unit/get_unit_by_id/$1';
$route['unit']['POST']              = 'unit/create_unit';
$route['unit']['PUT']               = 'unit/update_unit';
$route['unit/(:any)']['DELETE']     = 'unit/delete_unit/$1';

#variasi1
$route['variasi1']['GET']               = 'variasi1/get_variasi1';
$route['variasi1/all']['GET']           = 'variasi1/get_variasi1_all';
$route['variasi1/by-id/(:any)']['GET']  = 'variasi1/get_variasi1_by_id/$1';
$route['variasi1']['POST']              = 'variasi1/create_variasi1';
$route['variasi1']['PUT']               = 'variasi1/update_variasi1';
$route['variasi1/(:any)']['DELETE']     = 'variasi1/delete_variasi1/$1';

#variasi2
$route['variasi2']['GET']               = 'variasi2/get_variasi2';
$route['variasi2/all']['GET']           = 'variasi2/get_variasi2_all';
$route['variasi2/by-id/(:any)']['GET']  = 'variasi2/get_variasi2_by_id/$1';
$route['variasi2']['POST']              = 'variasi2/create_variasi2';
$route['variasi2']['PUT']               = 'variasi2/update_variasi2';
$route['variasi2/(:any)']['DELETE']     = 'variasi2/delete_variasi2/$1';

#item
$route['item']['GET']               = 'item/get_item';
$route['item/by-id/(:any)']['GET']  = 'item/get_item_by_id/$1';
$route['item']['POST']              = 'item/create_item';
$route['item']['PUT']               = 'item/update_item';
$route['item/(:any)']['DELETE']     = 'item/delete_item/$1';

#Pembelian
$route['pembelian']['GET']               = 'pembelian/get_pembelian';
$route['pembelian/by-id/(:any)']['GET']  = 'pembelian/get_pembelian_by_id/$1';
$route['pembelian']['POST']              = 'pembelian/create_pembelian';

#Stok
$route['stok']['GET']               = 'stok/get_stok';
$route['stok/by-id/(:any)']['GET']  = 'stok/get_stok_by_id/$1';

#Transfer Masuk
$route['transfer_masuk']['GET']               = 'transfer_masuk/get_transfer_masuk';
$route['transfer_masuk/by-id/(:any)']['GET']  = 'transfer_masuk/get_transfer_masuk_by_id/$1';
$route['transfer_masuk']['POST']              = 'transfer_masuk/create_transfer_masuk';

#Transfer Keluar
$route['transfer_keluar']['GET']               = 'transfer_keluar/get_transfer_keluar';
$route['transfer_keluar/by-id/(:any)']['GET']  = 'transfer_keluar/get_transfer_keluar_by_id/$1';
$route['transfer_keluar']['POST']              = 'transfer_keluar/create_transfer_keluar';

#Jenis Konsumen
$route['jenis_konsumen']['GET']               = 'jenis_konsumen/get_jenis_konsumen';
$route['jenis_konsumen/all']['GET']           = 'jenis_konsumen/get_jenis_konsumen_all';
$route['jenis_konsumen/by-id/(:any)']['GET']  = 'jenis_konsumen/get_jenis_konsumen_by_id/$1';
$route['jenis_konsumen']['POST']              = 'jenis_konsumen/create_jenis_konsumen';
$route['jenis_konsumen']['PUT']               = 'jenis_konsumen/update_jenis_konsumen';
$route['jenis_konsumen/(:any)']['DELETE']     = 'jenis_konsumen/delete_jenis_konsumen/$1';

#Jenis Transaksi
$route['jenis_transaksi']['GET']               = 'jenis_transaksi/get_jenis_transaksi';
$route['jenis_transaksi/all']['GET']           = 'jenis_transaksi/get_jenis_transaksi_all';
$route['jenis_transaksi/by-id/(:any)']['GET']  = 'jenis_transaksi/get_jenis_transaksi_by_id/$1';
$route['jenis_transaksi']['POST']              = 'jenis_transaksi/create_jenis_transaksi';
$route['jenis_transaksi']['PUT']               = 'jenis_transaksi/update_jenis_transaksi';
$route['jenis_transaksi/(:any)']['DELETE']     = 'jenis_transaksi/delete_jenis_transaksi/$1';

#Konsumen
$route['konsumen']['GET']               = 'konsumen/get_konsumen';
$route['konsumen/all']['GET']           = 'konsumen/get_konsumen_all';
$route['konsumen/by-id/(:any)']['GET']  = 'konsumen/get_konsumen_by_id/$1';
$route['konsumen']['POST']              = 'konsumen/create_konsumen';
$route['konsumen']['PUT']               = 'konsumen/update_konsumen';
$route['konsumen/(:any)']['DELETE']     = 'konsumen/delete_konsumen/$1';
