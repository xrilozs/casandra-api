<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Jenis_transaksi extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /jenis-transaksi [GET]
    function get_jenis_transaksi(){
        #check token
        $resp       = new Response_api();
        $header     = $this->input->request_headers();
        $verify_resp= verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-transaksi [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/jenis-transaksi [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get jenis transaksi
        $start          = $page_number * $page_size;
        $order          = array('field' => 'created_at', 'order' => 'DESC');
        if($order_by && $ordering){
            $order      = array('field' => $order_by, 'order' => $ordering);
        }
        $limit          = array('start' => $start, 'size' => $page_size);
        $jenis_transaksi = $this->jenis_transaksi_model->get_jenis_transaksi($search, $order, $limit);
        $records_total  = $this->jenis_transaksi_model->count_jenis_transaksi($search);

        if(empty($draw)){
            logging('debug', '/jenis-transaksi [GET] - Get jenis transaksi is success', $jenis_transaksi);
            $resp->set_response(200, "success", "Get jenis transaksi is success", $jenis_transaksi);
            set_output($resp->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/jenis-transaksi [GET] - Get jenis transaksi is success');
            $resp->set_response_datatable(200, $jenis_transaksi, $draw, $records_total, $records_total);
            set_output($resp->get_response_datatable());
            return;
        } 
    }

    #path: /jenis-transaksi/all [GET]
    function get_jenis_transaksi_all(){
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-transaksi/all [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get jenis transaksi detail
        $order          = array('field' => 'nama', 'order' => 'ASC');
        $jenis_transaksi = $this->jenis_transaksi_model->get_jenis_transaksi(null, $order);

        #response
        logging('debug', '/jenis-transaksi/all [GET] - Get jenis transaksi all success');
        $resp->set_response(200, "success", "Get jenis transaksi all success", $jenis_transaksi);
        set_output($resp->get_response());
        return;
    }

    #path: /jenis-transaksi/by-id/$id [GET]
    function get_jenis_transaksi_by_id($id){
        #check token
        $resp        = new Response_api();
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-transaksi/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get jenis transaksi detail
        $jenis_transaksi = $this->jenis_transaksi_model->get_jenis_transaksi_by_id($id);
        if(is_null($jenis_transaksi)){
            logging('error', '/jenis-transaksi/by-id/'.$id.' [GET] - jenis transaksi not found');
            $resp->set_response(404, "failed", "unit not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/jenis-transaksi/by-id/'.$id.' [GET] - Get jenis transaksi by id success', $jenis_transaksi);
        $resp->set_response(200, "success", "Get jenis transaksi by id success", $jenis_transaksi);
        set_output($resp->get_response());
        return;
    }

    #path: /jenis-transaksi [POST]
    function create_jenis_transaksi(){
        #check token
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-transaksi [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('nama', 'kode');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/jenis-transaksi [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create jenis transaksi
        $flag = $this->jenis_transaksi_model->create_jenis_transaksi($request);
        
        #response
        if(!$flag){
            logging('error', '/jenis-transaksi [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/jenis-transaksi [POST] - Create jenis transaksi success', $request);
        $resp->set_response(200, "success", "Create jenis transaksi success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /jenis-transaksi [PUT]
    function update_jenis_transaksi(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-transaksi [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'nama', 'kode');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/jenis-transaksi [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check jenis transaksi
        $jenis_transaksi = $this->jenis_transaksi_model->get_jenis_transaksi_by_id($request['id']);
        if(is_null($jenis_transaksi)){
            logging('error', '/jenis-transaksi [PUT] - jenis transaksi not found', $request);
            $resp->set_response(404, "failed", "unit not found");
            set_output($resp->get_response());
            return;
        }

        #update jenis transaksi
        $flag = $this->jenis_transaksi_model->update_jenis_transaksi($request);
        
        #response
        if(empty($flag)){
            logging('error', '/jenis-transaksi [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/jenis-transaksi [PUT] - Update jenis transaksi success', $request);
        $resp->set_response(200, "success", "Update jenis transaksi success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /jenis-transaksi/$id [DELETE]
    function delete_jenis_transaksi($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-transaksi/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check jenis transaksi
        $jenis_transaksi = $this->jenis_transaksi_model->get_jenis_transaksi_by_id($id);
        if(is_null($jenis_transaksi)){
            logging('error', '/jenis-transaksi/'.$id.' [DELETE] - jenis transaksi not found');
            $resp->set_response(404, "failed", "unit not found");
            set_output($resp->get_response());
            return;
        }

        #active jenis transaksi
        $flag = $this->jenis_transaksi_model->delete_jenis_transaksi($id);
        
        #response
        if(empty($flag)){
            logging('error', '/jenis-transaksi/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/jenis-transaksi/'.$id.' [DELETE] - delete jenis transaksi success');
        $resp->set_response(200, "success", "delete jenis transaksi success");
        set_output($resp->get_response());
        return;
    }

}
