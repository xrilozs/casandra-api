<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Variasi1 extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /variasi1 [GET]
    function get_variasi1(){
        #check token
        $resp       = new Response_api();
        $header     = $this->input->request_headers();
        $verify_resp= verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi1/change-password [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/variasi1 [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get variasi1
        $start          = $page_number * $page_size;
        $order          = array('field' => 'created_at', 'order' => 'DESC');
        if($order_by && $ordering){
            $order      = array('field' => $order_by, 'order' => $ordering);
        }
        $limit          = array('start' => $start, 'size' => $page_size);
        $variasi1       = $this->variasi1_model->get_variasi1($search, $order, $limit);
        $records_total  = $this->variasi1_model->count_variasi1($search);

        if(empty($draw)){
            logging('debug', '/variasi1 [GET] - Get variasi1 is success', $variasi1);
            $resp->set_response(200, "success", "Get variasi1 is success", $variasi1);
            set_output($resp->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/variasi1 [GET] - Get variasi1 is success');
            $resp->set_response_datatable(200, $variasi1, $draw, $records_total, $records_total);
            set_output($resp->get_response_datatable());
            return;
        } 
    }

    #path: /variasi1/all [GET]
    function get_variasi1_all(){
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi1/all [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get variasi1 detail
        $order    = array('field' => 'warna', 'order' => 'ASC');
        $variasi1 = $this->variasi1_model->get_variasi1(null, $order);

        #response
        logging('debug', '/variasi1/all [GET] - Get variasi1 all success');
        $resp->set_response(200, "success", "Get variasi1 all success", $variasi1);
        set_output($resp->get_response());
        return;
    }

    #path: /variasi1/by-id/$id [GET]
    function get_variasi1_by_id($id){
        #check token
        $resp        = new Response_api();
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi1/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get variasi1 detail
        $variasi1 = $this->variasi1_model->get_variasi1_by_id($id);
        if(is_null($variasi1)){
            logging('error', '/variasi1/by-id/'.$id.' [GET] - variasi1 not found');
            $resp->set_response(404, "failed", "variasi1 not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/variasi1/by-id/'.$id.' [GET] - Get variasi1 by id success', $variasi1);
        $resp->set_response(200, "success", "Get variasi1 by id success", $variasi1);
        set_output($resp->get_response());
        return;
    }

    #path: /variasi1 [POST]
    function create_variasi1(){
        #check token
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi1 [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('warna');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/variasi1 [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create variasi1
        $flag = $this->variasi1_model->create_variasi1($request);
        
        #response
        if(!$flag){
            logging('error', '/variasi1 [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/variasi1 [POST] - Create variasi1 success', $request);
        $resp->set_response(200, "success", "Create variasi1 success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /variasi1 [PUT]
    function update_variasi1(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi1 [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'warna');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/variasi1 [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check variasi1
        $variasi1 = $this->variasi1_model->get_variasi1_by_id($request['id']);
        if(is_null($variasi1)){
            logging('error', '/variasi1 [PUT] - variasi1 not found', $request);
            $resp->set_response(404, "failed", "variasi1 not found");
            set_output($resp->get_response());
            return;
        }

        #update variasi1
        $flag = $this->variasi1_model->update_variasi1($request);
        
        #response
        if(empty($flag)){
            logging('error', '/variasi1 [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/variasi1 [PUT] - Update variasi1 success', $request);
        $resp->set_response(200, "success", "Update variasi1 success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /variasi1/$id [DELETE]
    function delete_variasi1($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi1/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check variasi1
        $variasi1 = $this->variasi1_model->get_variasi1_by_id($id);
        if(is_null($variasi1)){
            logging('error', '/variasi1/'.$id.' [DELETE] - variasi1 not found');
            $resp->set_response(404, "failed", "variasi1 not found");
            set_output($resp->get_response());
            return;
        }

        #active variasi1
        $flag = $this->variasi1_model->delete_variasi1($id);
        
        #response
        if(empty($flag)){
            logging('error', '/variasi1/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/variasi1/'.$id.' [DELETE] - delete variasi1 success');
        $resp->set_response(200, "success", "delete variasi1 success");
        set_output($resp->get_response());
        return;
    }

}
