<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Pembelian extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /pembelian [GET]
    function get_pembelian(){
        #init variable
        $resp           = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $date           = $this->input->get('date');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        $params         = array($page_number, $page_size);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/pembelian [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $operator           = $verify_resp['data'];
        $operator_lokasi    = $this->operator_lokasi_model->get_operator_lokasi_by_operator_id($operator->id);
        $lokasi             = array_map(function($item) {
            return $item->lokasi_id;
        }, $operator_lokasi);
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/pembelian [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get pembelian
        $start          = $page_number * $page_size;
        $order          = array('field'=>'created_at', 'order'=>'DESC');
        if($order_by && $ordering){
            $order = array('field'=>$order_by, 'order'=>$ordering);
        }
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $pembelian      = $this->pembelian_detail_model->get_pembelian_detail($search, $date, $lokasi, $order, $limit);
        $records_total  = $this->pembelian_detail_model->count_pembelian_detail($search, $date, $lokasi);
        
        #response
        if(empty($draw)){
          logging('debug', '/pembelian [GET] - Get pembelian is success', $pembelian);
          $resp->set_response(200, "success", "Get pembelian is success", $pembelian);
          set_output($resp->get_response());
          return;
        }else{
          logging('debug', '/pembelian [GET] - Get pembelian is success');
          $resp->set_response_datatable(200, $pembelian, $draw, $records_total, $records_total);
          set_output($resp->get_response_datatable());
          return;
        } 
    }

    #path: /pembelian/by-id/$id [GET]
    function get_pembelian_by_id($id){
        $resp        = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/pembelian/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get pembelian by id
        $pembelian = $this->pembelian_detail_model->get_pembelian_detail_by_id($id);
        if(is_null($pembelian)){
            logging('error', '/pembelian/by-id/'.$id.' [GET] - pembelian not found');
            $resp->set_response(404, "failed", "pembelian not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/pembelian/by-id/'.$id.' [GET] - Get pembelian by id success', $pembelian);
        $resp->set_response(200, "success", "Get pembelian by id success", $pembelian);
        set_output($resp->get_response());
        return;
    }
  
    #path: /pembelian [POST]
    function create_pembelian(){
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $allowed_role   = array('owner');
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/pembelian [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $operator = $verify_resp['data'];
        
        #check request params
        $keys = array('no_faktur', 'suplier_id', 'lokasi_id', 'items');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/pembelian [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }
        $pembelian_detail = $request['items'];
        foreach ($pembelian_detail as $item) {
            $item_keys = array('kode_barang', 'harga', 'qty', 'harga_pokok');
            if(!check_parameter_by_keys($item, $item_keys)){
                logging('error', '/api/pembelian [POST] - Missing parameter. please check API documentation', $request);
                $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
                set_output($resp_obj->get_response());
                return;
            }
        }

        #check lokasi
        $list_lokasi        = $this->operator_lokasi_model->get_operator_lokasi_by_operator_id($operator->id);
        $operator_lokasi_id = array_map(function($item) {
            return $item->lokasi_id;
        }, $list_lokasi);
        if(!in_array($request['lokasi_id'], $operator_lokasi_id)){
            logging('error', '/api/pembelian [POST] - Invalid Lokasi', $request);
            $resp_obj->set_response(400, "failed", "Invalid Lokasi");
            set_output($resp_obj->get_response());
            return;
        }

        $this->db->trans_begin();
        try{
            #request
            $pembelian_id   = get_uniq_id();
            $pembelian      = array(
                "id"            => $pembelian_id,
                "no_faktur"     => $request['no_faktur'],
                "tanggal"       => date("Y-m-d"),
                "operator_id"   => $operator->id,
                "suplier_id"    => $request['suplier_id'],
                "lokasi_id"     => $request['lokasi_id'],
                "keterangan"    => array_key_exists("keterangan", $request) ? $request['keterangan'] : null,
                "created_at"    => date("Y-m-d H:i:s")
            );

            #create pembelian
            $flag = $this->pembelian_model->create_pembelian($pembelian);
            
            #response
            if(!$flag){
                $this->db->trans_rollback();
                logging('error', '/pembelian [POST] - Internal server error', $request);
                $resp->set_response(500, "failed", "Internal server error");
                set_output($resp->get_response());
                return;
            }

            foreach ($pembelian_detail as $item) {
                $pembelian_item = array(
                    "id"            => get_uniq_id(),
                    "no_faktur"     => $request['no_faktur'],
                    "kode_barang"   => $item['kode_barang'],
                    "harga"         => $item['harga'],
                    "qty"           => $item['qty'],
                    "diskon"        => array_key_exists("diskon", $item) ? $item['diskon'] : null,
                    "ppn"           => array_key_exists("ppn", $item) ? $item['ppn'] : null,
                    "harga_pokok"   => $item['harga_pokok'],
                    "created_at"    => date("Y-m-d H:i:s")
                );
                #create pembelian detail
                $flag = $this->pembelian_detail_model->create_pembelian_detail($pembelian_item);
                
                #response
                if(!$flag){
                    $this->db->trans_rollback();
                    logging('error', '/pembelian [POST] - Internal server error', $request);
                    $resp->set_response(500, "failed", "Internal server error");
                    set_output($resp->get_response());
                    return;
                }
            }

            

            #create/update stock item
            $is_failed = 0;
            foreach($pembelian_detail as $item){
                $stock = $this->stok_model->get_stok_by_kode_and_lokasi($item['kode_barang'], $request['lokasi_id']);
                if($stock){
                    $rata_beli = $this->pembelian_detail_model->get_rata_beli_by_kode_and_lokasi($item['kode_barang'], $request['lokasi_id']);
                    $stok = array(
                        "id"            => $stock->id,
                        "lokasi_id"     => $stock->lokasi_id,
                        "kode_barang"   => $stock->kode_barang,
                        "stok"          => intval($stock->stok) + intval($item['qty']),
                        "rata_beli"     => $rata_beli->rata_beli,
                        "harga_beli"    => $item['harga'],
                        "harga_jual"    => $stock->harga_jual,
                        "harga_grosir"  => $stock->harga_grosir,
                        "harga_pokok"   => $item['harga_pokok'],
                        "harga_retail"  => $stock->harga_retail,
                        "harga_reseller"=> $stock->harga_reseller,
                        "opname"        => $stock->opname,
                        "acc"           => $stock->acc,
                    );
                    $flag = $this->stok_model->update_stok($stok);
                    if(!$flag){
                        $is_failed = 1;
                        break;
                    }
                }else{
                    $stok = array(
                        "id"            => get_uniq_id(),
                        "lokasi_id"     => $request['lokasi_id'],
                        "kode_barang"   => $item['kode_barang'],
                        "stok"          => $item['qty'],
                        "rata_beli"     => $item['harga'],
                        "harga_beli"    => $item['harga'],
                        "harga_pokok"   => $item['harga_pokok'],
                        "created_at"    => date("Y-m-d H:i:s")
                    );
                    $flag = $this->stok_model->create_stok($stok);
                    if(!$flag){
                        $is_failed = 1;
                        break;
                    }
                }
            }
            if($is_failed){
                $this->db->trans_rollback();
                logging('debug', '/pembelian [POST] - Create pembelian failed. Update stock failed!');
                $resp->set_response(500, "failed", "Internal server error");
                set_output($resp->get_response());
                return;
            }
        }catch(Exception $e){
            $this->db->trans_rollback();
            logging('debug', '/pembelian [POST] - Create pembelian failed', $e);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        $this->db->trans_commit();

        logging('debug', '/pembelian [POST] - Create pembelian success', $request);
        $resp->set_response(200, "success", "Create pembelian success", $request);
        set_output($resp->get_response());
        return;
    }
}