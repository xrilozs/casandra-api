<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Operator extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /operator/login [POST]
    function login(){
        #init req & resp
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check request payload
        $keys = array('username', 'password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/operator/login [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #init variable
        $username   = $request['username'];
        $password   = $request['password'];

        #get operator
        $operator = $this->operator_model->get_operator_by_username($username);
        if(is_null($operator)){
            logging('error', '/operator/login [POST] - operator not found', $request);
            $resp->set_response(404, "failed", "operator not found");
            set_output($resp->get_response());
            return;
        }

        #check operator
        $verify = password_verify($password, $operator->password);
        if(!$verify){
            logging('error', '/operator/login [POST] - Invalid password', $request);
            $resp->set_response(400, "failed", "Invalid password");
            set_output($resp->get_response());
            return;
        }

        #Create operator token      
        $token_expired = time() + (10 * 60);
        $payload = array(
          'username'    => $username,
          'exp'         => $token_expired
        );
        #access token
        $access_token   = JWT::encode($payload, ACCESS_TOKEN_SECRET);
        #refresh token
        $payload['exp'] = time() + (60 * 60);
        $refresh_token  = JWT::encode($payload, REFRESH_TOKEN_SECRET);
      
        $response = array(
          'access_token'    => $access_token,
          'expiry'          => date(DATE_ISO8601, $token_expired),
          'refresh_token'   => $refresh_token,
          'role'            => $operator->role,
          'username'        => $operator->username
        );
        
        logging('debug', '/operator/login [POST] - operator login success', $response);
        $resp->set_response(200, "success", "operator login success", $response);
        set_output($resp->get_response());
        return;
    }

    #path: operator/refresh [GET]
    function refresh(){
      #check header
      $resp     = new Response_api();
      $header   = $this->input->request_headers();
      if(isset($header['Authorization'])){
        list(, $token) = explode(' ', $header['Authorization']);
      }else{
        logging('debug', '/operator/refresh [GET] - Please use token to access this resource.');
        $resp->set_response(401, "failed", "Please use token to access this resource.");
        set_output($resp->get_response());
        return;
      }

      try {
        $jwt = JWT::decode($token, REFRESH_TOKEN_SECRET, ['HS256']);
      } catch (Exception $e) {
        logging('debug', '/operator/refresh [GET] - Invalid requested token');
        $resp->set_response(401, "failed", "Invalid requested token");
        set_output($resp->get_response());
        return;
      }
      $username = $jwt->username;
      $operator = $this->operator_model->get_operator_by_username($username);

      #check operator exist
      if(is_null($operator)){
        logging('debug', '/operator/refresh [GET] - operator not found');
        $resp->set_response(401, "failed", "operator not found");
        set_output($resp->get_response());
        return;
      }
      
      #generate new token
      $token_expired = time() + (10 * 60);
      $payload = array(
        'username'  => $username,
        'exp'       => $token_expired
      );
      #access token
      $access_token     = JWT::encode($payload, ACCESS_TOKEN_SECRET);
      #refresh token
      $payload['exp']   = time() + (60 * 60);
      $refresh_token    = JWT::encode($payload, REFRESH_TOKEN_SECRET);

      $response = array(
        'access_token'  => $access_token,
        'expiry'        => date(DATE_ISO8601, $token_expired),
        'refresh_token' => $refresh_token,
        'role'          => $operator->role,
        'username'      => $operator->username
      );
      
      logging('debug', '/operator/refresh [GET] - Refresh operator success', $response);
      $resp->set_response(200, "success", "Refresh operator success", $response);
      set_output($resp->get_response());
      return;
    }

    #path: /operator/change-password [PUT]
    function change_password(){
        #check token
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        $header     = $this->input->request_headers();
        $verify_resp= verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/operator/change-password [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $operator = $verify_resp['data'];

        #check request params
        $keys = array('old_password', 'new_password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/operator/change-password [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }
      
        if(!password_verify($request['old_password'], $operator->password)){
            logging('error', '/operator/change-password [PUT] - Old password invalid', $request);
            $resp->set_response(400, "failed", "Old password invalid");
            set_output($resp->get_response());
            return;
        }

        #update operator
        $hash                   = password_hash($request['new_password'], PASSWORD_DEFAULT);
        $request['password']    = $hash;
        $request['id']          = $operator->id;
        $flag                   = $this->operator_model->change_password($operator->id, $hash);
        
        #response
        if(empty($flag)){
            logging('error', '/operator/change-password [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/operator/change-password [PUT] - Change password operator success', $request);
        $resp->set_response(200, "success", "Change password operator success");
        set_output($resp->get_response());
        return;
    }

    #path: /operator [GET]
    function get_operator(){
        #init variable
        $resp           = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $role           = $this->input->get('role');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        $params         = array($page_number, $page_size);
        $allowed_role   = array('owner');

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/operator [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $operator = $verify_resp['data'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/operator [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get operator
        $start          = $page_number * $page_size;
        $order          = array('field'=>'created_at', 'order'=>'DESC');
        if($order_by && $ordering){
            $order      = array('field'=>$order_by, 'order'=>$ordering);
        }
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $operator       = $this->operator_model->get_operators($search, $role, $order, $limit);
        $records_total  = $this->operator_model->count_operator($search, $role);

        foreach ($operator as &$item) {
            $operator_lokasi = $this->operator_lokasi_model->get_operator_lokasi_by_operator_id($item->id);
            $item->operator_lokasi = $operator_lokasi;
        }
        
        #response
        if(empty($draw)){
          logging('debug', '/operator [GET] - Get operator is success', $operator);
          $resp->set_response(200, "success", "Get operator is success", $operator);
          set_output($resp->get_response());
          return;
        }else{
          logging('debug', '/operator [GET] - Get operator is success');
          $resp->set_response_datatable(200, $operator, $draw, $records_total, $records_total);
          set_output($resp->get_response_datatable());
          return;
        } 
    }

    #path: /operator/by-id/$id [GET]
    function get_operator_by_id($id){
        $resp           = new Response_api();
        $allowed_role   = array('owner');

        #check token
        $header         = $this->input->request_headers();
        $verify_resp = verify_operator_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/operator/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get operator by id
        $operator = $this->operator_model->get_operator_by_id($id);
        if(is_null($operator)){
            logging('error', '/operator/by-id/'.$id.' [GET] - operator not found');
            $resp->set_response(404, "failed", "operator not found");
            set_output($resp->get_response());
            return;
        }
        unset($operator->password);
        $operator_lokasi = $this->operator_lokasi_model->get_operator_lokasi_by_operator_id($operator->id);
        $operator->operator_lokasi = $operator_lokasi;

        #response
        logging('debug', '/operator/by-id/'.$id.' [GET] - Get operator by id success', $operator);
        $resp->set_response(200, "success", "Get operator by id success", $operator);
        set_output($resp->get_response());
        return;
    }

    #path: /operator/profile [GET]
    function get_profile(){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/operator/profile [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $operator = $verify_resp['data'];
        unset($operator->password);
        $operator_lokasi = $this->operator_lokasi_model->get_operator_lokasi_by_operator_id($operator->id);
        $operator->operator_lokasi = $operator_lokasi;

        #response
        logging('debug', '/operator/profile [GET] - Get profile success', $operator);
        $resp->set_response(200, "success", "Get profile success", $operator);
        set_output($resp->get_response());
        return;
    }
  
    #path: /operator [POST]
    function create_operator(){
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $allowed_role   = array('owner');
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/operator [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('username', 'password', 'role', 'lokasi');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/operator [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check duplicate
        $operator = $this->operator_model->get_operator_by_username($request['username']);
        if($operator){
            logging('error', '/operator [POST] - Username already registered', $request);
            $resp->set_response(400, "failed", "Username already registered");
            set_output($resp->get_response());
            return;
        }

        #create operator
        $hash                   = password_hash($request['password'], PASSWORD_DEFAULT);
        $request['password']    = $hash;
        $operator_id            = $this->operator_model->create_operator($request);
        
        #response
        if(!$operator_id){
            logging('error', '/operator [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        #create operator lokasi
        $lokasi = $request['lokasi'];
        foreach($lokasi as $item){
            $operator_lokasi = array(
                "lokasi_id" => $item,
                "operator_id" => $operator_id
            );
            $this->operator_lokasi_model->create_operator_lokasi($operator_lokasi);
        }

        unset($request['password']);
        logging('debug', '/operator [POST] - Create operator success', $request);
        $resp->set_response(200, "success", "Create operator success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /operator [PUT]
    function update_operator(){
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $allowed_role   = array('owner');

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/operator [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'username', 'role', 'lokasi');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/operator [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check operator
        $operator = $this->operator_model->get_operator_by_id($request['id']);
        if(is_null($operator)){
            logging('error', '/operator [PUT] - operator not found', $request);
            $resp->set_response(404, "failed", "operator not found");
            set_output($resp->get_response());
            return;
        }

        #check changes of username
        if($operator->username != $request['username']){
            #check duplicate username
            $operator_exist = $this->operator_model->get_operator_by_username($request['username']);
            if($operator_exist){
                logging('error', '/operator [POST] - New username already registered', $request);
                $resp->set_response(400, "failed", "New username already registered");
                set_output($resp->get_response());
                return;
            }
        }

        #update operator
        $request['password'] = $operator->password;
        $flag                = $this->operator_model->update_operator($request);

        $req_lokasi         = $request['lokasi'];
        $list_lokasi        = $this->operator_lokasi_model->get_operator_lokasi_by_operator_id($operator->id);
        $exist_lokasi_id    = array_map(function($item) {
            return $item->lokasi_id;
        }, $list_lokasi);
        $deleted_lokasi_id  = array_diff($exist_lokasi_id, $req_lokasi);
        $new_lokasi_id      = array_diff($req_lokasi, $exist_lokasi_id);

        #create operator lokasi
        foreach($new_lokasi_id as $lokasi_id){
            $operator_lokasi = array(
                "lokasi_id"     => $lokasi_id,
                "operator_id"   => $operator->id
            );
            $this->operator_lokasi_model->create_operator_lokasi($operator_lokasi);
        }

        #delete operator lokasi
        foreach($deleted_lokasi_id as $lokasi_id){
            $this->operator_lokasi_model->delete_operator_lokasi2($operator->id, $lokasi_id);
        }
        
        unset($request['password']);
        logging('debug', '/operator [PUT] - Update operator success', $request);
        $resp->set_response(200, "success", "Update operator success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /operator/$id [DELETE]
    function delete_operator($id){
        #init variable
        $resp           = new Response_api();
        $allowed_role   = array('owner');

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/operator/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check operator
        $operator = $this->operator_model->get_operator_by_id($id);
        if(is_null($operator)){
            logging('error', '/operator/'.$id.' [DELETE] - operator not found');
            $resp->set_response(404, "failed", "operator not found");
            set_output($resp->get_response());
            return;
        }

        #active operator
        $flag = $this->operator_model->delete_operator($id);
        
        #response
        if(empty($flag)){
            logging('error', '/operator/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/operator/'.$id.' [DELETE] - delete operator success');
        $resp->set_response(200, "success", "delete operator success");
        set_output($resp->get_response());
        return;
    }
}