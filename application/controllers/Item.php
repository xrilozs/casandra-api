<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Item extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /item [GET]
    function get_item(){
        #check token
        $resp       = new Response_api();
        $header     = $this->input->request_headers();
        $verify_resp= verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/item [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $type           = $this->input->get('type');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/item [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get item
        $start = $page_number * $page_size;
        $order = array('field' => 'created_at', 'order' => 'DESC');
        if($order_by && $ordering){
            $order = array('field' => $order_by, 'order' => $ordering);
        }
        $limit = array('start' => $start, 'size' => $page_size);
        $items = $this->item_model->get_item($search, $type, $order, $limit);
        $total = $this->item_model->count_item($search, $type);

        foreach ($items as $item) {
            $item->satuan   = $item->kode_satuan ? $this->unit_model->get_unit_by_id($item->kode_satuan) : null;
            $item->kategori = $item->kode_kategori ? $this->kategori_model->get_kategori_by_id($item->kode_kategori) : null;
            $item->suplier    = $item->id_suplier ? $this->suplier_model->get_suplier_by_id($item->id_suplier) : null;
            $item->variasi1 = $item->kode_variasi1 ? $this->variasi1_model->get_variasi1_by_id($item->kode_variasi1) : null;
            $item->variasi2 = $item->kode_variasi2 ? $this->variasi2_model->get_variasi2_by_id($item->kode_variasi2) : null;
        }

        if(empty($draw)){
            logging('debug', '/item [GET] - Get item is success', $items);
            $resp->set_response(200, "success", "Get item is success", $items);
            set_output($resp->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/item [GET] - Get item is success');
            $resp->set_response_datatable(200, $items, $draw, $total, $total);
            set_output($resp->get_response_datatable());
            return;
        } 
    }

    #path: /item/by-id/$id [GET]
    function get_item_by_id($id){
        #check token
        $resp        = new Response_api();
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/item/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get item detail
        $item = $this->item_model->get_item_by_id($id);
        if(is_null($item)){
            logging('error', '/item/by-id/'.$id.' [GET] - item not found');
            $resp->set_response(404, "failed", "item not found");
            set_output($resp->get_response());
            return;
        }

        $master         = $item->status == 'Variasi' ? $this->item_model->get_item_by_kode($item->kode_master) : null;
        $kode_satuan    = $master ? $master->kode_satuan : $item->kode_satuan;
        $kode_kategori  = $master ? $master->kode_kategori : $item->kode_kategori;
        $id_suplier    = $master ? $master->id_suplier : $item->id_suplier;

        $item->satuan   = $this->unit_model->get_unit_by_id($kode_satuan);
        $item->kategori = $this->kategori_model->get_kategori_by_id($kode_kategori);
        $item->suplier  = $this->suplier_model->get_suplier_by_id($id_suplier);
        $item->variasi1 = is_null($master) ? $this->variasi1_model->get_variasi1_by_id($item->kode_variasi1) : null;
        $item->variasi2 = is_null($master) ? $this->variasi2_model->get_variasi2_by_id($item->kode_variasi2) : null;

        #response
        logging('debug', '/item/by-id/'.$id.' [GET] - Get item by id success', $item);
        $resp->set_response(200, "success", "Get item by id success", $item);
        set_output($resp->get_response());
        return;
    }

    #path: /item [POST]
    function create_item(){
        #check token
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/item [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('kode', 'status');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/item [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        if($request['status'] == 'Master'){
            #check request params
            $keys = array('model', 'kode_satuan', 'kode_kategori', 'id_suplier');
            if(!check_parameter_by_keys($request, $keys)){
                logging('error', '/item [POST] - Missing parameter. please check API documentation', $request);
                $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
                set_output($resp->get_response());
                return;
            }

            $request['nama_barang'] = $request['model'];
        }else{
            #check request params
            $keys = array('kode_master');
            if(!check_parameter_by_keys($request, $keys)){
                logging('error', '/item [POST] - Missing parameter. please check API documentation', $request);
                $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
                set_output($resp->get_response());
                return;
            }
            if(!isset($request['kode_variasi1']) && !isset($request['kode_variasi2'])){
                logging('error', '/item [POST] - Missing parameter. please check API documentation', $request);
                $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
                set_output($resp->get_response());
                return;
            }

            $master = $this->item_model->get_item_by_kode($request['kode_master']);
            if(is_null($master)){
                logging('error', '/item [POST] - Invalid kode master', $request);
                $resp->set_response(400, "failed", "Invalid kode master");
                set_output($resp->get_response());
                return;
            }

            $variasi1_id = isset($request['kode_variasi1']) ? $request['kode_variasi1'] : null;
            $variasi2_id = isset($request['kode_variasi2']) ? $request['kode_variasi2'] : null;
            $nama_barang = $master->model;
            if($variasi1_id){
                $variasi1       = $this->variasi1_model->get_variasi1_by_id($variasi1_id); 
                $nama_barang   .= " $variasi1->warna";
            }
            if($variasi2_id){
                $variasi2       = $this->variasi2_model->get_variasi2_by_id($variasi2_id); 
                $nama_barang   .= " $variasi2->ukuran";
            }
            $request['model']       = $master->model;
            $request['nama_barang'] = $nama_barang;
        }

        #create item
        $request['id']  = get_uniq_id();
        $flag           = $this->item_model->create_item($request);
        
        #response
        if(!$flag){
            logging('error', '/item [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/item [POST] - Create item success', $request);
        $resp->set_response(200, "success", "Create item success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /item [PUT]
    function update_item(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/item [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/item [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check item
        $item = $this->item_model->get_item_by_id($request['id']);
        if(is_null($item)){
            logging('error', '/item [PUT] - item not found', $request);
            $resp->set_response(404, "failed", "item not found");
            set_output($resp->get_response());
            return;
        }

        if($item->status == 'Master'){
            #check request params
            $keys = array('model', 'kode_satuan', 'kode_kategori', 'id_suplier');
            if(!check_parameter_by_keys($request, $keys)){
                logging('error', '/item [POST] - Missing parameter. please check API documentation', $request);
                $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
                set_output($resp->get_response());
                return;
            }

            $request['kode']        = $item->kode;
            $request['nama_barang'] = $request['model'];
            $request['status']        = $item->status;
        }else{
            #check request params
            if(!isset($request['kode_variasi1']) && !isset($request['kode_variasi2'])){
                logging('error', '/item [POST] - Missing parameter. please check API documentation', $request);
                $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
                set_output($resp->get_response());
                return;
            }

            $master      = $this->item_model->get_item_by_kode($item->kode_master);
            $variasi1_id = isset($request['kode_variasi1']) ? $request['kode_variasi1'] : null;
            $variasi2_id = isset($request['kode_variasi2']) ? $request['kode_variasi2'] : null;
            $nama_barang = $master->model;
            if($variasi1_id){
                $variasi1       = $this->variasi1_model->get_variasi1_by_id($variasi1_id); 
                $nama_barang   .= " $variasi1->warna";
            }
            if($variasi2_id){
                $variasi2       = $this->variasi2_model->get_variasi2_by_id($variasi2_id); 
                $nama_barang   .= " $variasi2->ukuran";
            }

            $request['kode']        = $item->kode;
            $request['kode_master'] = $master->kode;
            $request['model']       = $master->model;
            $request['nama_barang'] = $nama_barang;
            $request['status']      = $item->status;
        }

        #update item
        $flag = $this->item_model->update_item($request);
        
        #response
        if(empty($flag)){
            logging('error', '/item [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/item [PUT] - Update item success', $request);
        $resp->set_response(200, "success", "Update item success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /item/$id [DELETE]
    function delete_item($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/item/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check item
        $item = $this->item_model->get_item_by_id($id);
        if(is_null($item)){
            logging('error', '/item/'.$id.' [DELETE] - item not found');
            $resp->set_response(404, "failed", "item not found");
            set_output($resp->get_response());
            return;
        }

        $variants = $this->item_model->get_item_by_kode_master($item->kode);
        if(sizeof($variants) > 0){
            logging('error', '/item/'.$id.' [DELETE] - Cannot delete master Item. Item has variant');
            $resp->set_response(400, "failed", "Cannot delete master Item. Item has variant");
            set_output($resp->get_response());
            return;
        }

        #active item
        $flag = $this->item_model->delete_item($id);
        
        #response
        if(empty($flag)){
            logging('error', '/item/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/item/'.$id.' [DELETE] - delete item success');
        $resp->set_response(200, "success", "delete item success");
        set_output($resp->get_response());
        return;
    }

}
