<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Variasi2 extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /variasi2 [GET]
    function get_variasi2(){
        #check token
        $resp       = new Response_api();
        $header     = $this->input->request_headers();
        $verify_resp= verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi2/change-password [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/variasi2 [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get variasi2
        $start          = $page_number * $page_size;
        $order          = array('field' => 'created_at', 'order' => 'DESC');
        if($order_by && $ordering){
            $order      = array('field' => $order_by, 'order' => $ordering);
        }
        $limit          = array('start' => $start, 'size' => $page_size);
        $variasi2       = $this->variasi2_model->get_variasi2($search, $order, $limit);
        $records_total  = $this->variasi2_model->count_variasi2($search);

        if(empty($draw)){
            logging('debug', '/variasi2 [GET] - Get variasi2 is success', $variasi2);
            $resp->set_response(200, "success", "Get variasi2 is success", $variasi2);
            set_output($resp->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/variasi2 [GET] - Get variasi2 is success');
            $resp->set_response_datatable(200, $variasi2, $draw, $records_total, $records_total);
            set_output($resp->get_response_datatable());
            return;
        } 
    }

    #path: /variasi2/all [GET]
    function get_variasi2_all(){
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi2/all [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get variasi2 detail
        $order    = array('field' => 'ukuran', 'order' => 'ASC');
        $variasi2 = $this->variasi2_model->get_variasi2(null, $order);

        #response
        logging('debug', '/variasi2/all [GET] - Get variasi2 all success');
        $resp->set_response(200, "success", "Get variasi2 all success", $variasi2);
        set_output($resp->get_response());
        return;
    }

    #path: /variasi2/by-id/$id [GET]
    function get_variasi2_by_id($id){
        #check token
        $resp        = new Response_api();
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi2/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get variasi2 detail
        $variasi2 = $this->variasi2_model->get_variasi2_by_id($id);
        if(is_null($variasi2)){
            logging('error', '/variasi2/by-id/'.$id.' [GET] - variasi2 not found');
            $resp->set_response(404, "failed", "variasi2 not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/variasi2/by-id/'.$id.' [GET] - Get variasi2 by id success', $variasi2);
        $resp->set_response(200, "success", "Get variasi2 by id success", $variasi2);
        set_output($resp->get_response());
        return;
    }

    #path: /variasi2 [POST]
    function create_variasi2(){
        #check token
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi2 [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('ukuran');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/variasi2 [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create variasi2
        $flag = $this->variasi2_model->create_variasi2($request);
        
        #response
        if(!$flag){
            logging('error', '/variasi2 [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/variasi2 [POST] - Create variasi2 success', $request);
        $resp->set_response(200, "success", "Create variasi2 success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /variasi2 [PUT]
    function update_variasi2(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi2 [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'ukuran');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/variasi2 [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check variasi2
        $variasi2 = $this->variasi2_model->get_variasi2_by_id($request['id']);
        if(is_null($variasi2)){
            logging('error', '/variasi2 [PUT] - variasi2 not found', $request);
            $resp->set_response(404, "failed", "variasi2 not found");
            set_output($resp->get_response());
            return;
        }

        #update variasi2
        $flag = $this->variasi2_model->update_variasi2($request);
        
        #response
        if(empty($flag)){
            logging('error', '/variasi2 [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/variasi2 [PUT] - Update variasi2 success', $request);
        $resp->set_response(200, "success", "Update variasi2 success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /variasi2/$id [DELETE]
    function delete_variasi2($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/variasi2/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check variasi2
        $variasi2 = $this->variasi2_model->get_variasi2_by_id($id);
        if(is_null($variasi2)){
            logging('error', '/variasi2/'.$id.' [DELETE] - variasi2 not found');
            $resp->set_response(404, "failed", "variasi2 not found");
            set_output($resp->get_response());
            return;
        }

        #active variasi2
        $flag = $this->variasi2_model->delete_variasi2($id);
        
        #response
        if(empty($flag)){
            logging('error', '/variasi2/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/variasi2/'.$id.' [DELETE] - delete variasi2 success');
        $resp->set_response(200, "success", "delete variasi2 success");
        set_output($resp->get_response());
        return;
    }

}
