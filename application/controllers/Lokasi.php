<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Lokasi extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /lokasi [GET]
    function get_lokasi(){
        #check token
        $resp       = new Response_api();
        $header     = $this->input->request_headers();
        $verify_resp= verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/lokasi/change-password [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/lokasi [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get lokasi
        $start          = $page_number * $page_size;
        $order          = array('field' => 'created_at', 'order' => 'DESC');
        if($order_by && $ordering){
            $order      = array('field' => $order_by, 'order' => $ordering);
        }
        $limit          = array('start' => $start, 'size' => $page_size);
        $lokasi       = $this->lokasi_model->get_lokasi($search, $order, $limit);
        $records_total  = $this->lokasi_model->count_lokasi($search);

        if(empty($draw)){
            logging('debug', '/lokasi [GET] - Get lokasi is success', $lokasi);
            $resp->set_response(200, "success", "Get lokasi is success", $lokasi);
            set_output($resp->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/lokasi [GET] - Get lokasi is success');
            $resp->set_response_datatable(200, $lokasi, $draw, $records_total, $records_total);
            set_output($resp->get_response_datatable());
            return;
        } 
    }

    #path: /lokasi/all [GET]
    function get_lokasi_all(){
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/lokasi/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get lokasi detail
        $order    = array('field' => 'lokasi', 'order' => 'ASC');
        $lokasi = $this->lokasi_model->get_lokasi(null, $order);

        #response
        logging('debug', '/lokasi/all [GET] - Get lokasi all success');
        $resp->set_response(200, "success", "Get lokasi all success", $lokasi);
        set_output($resp->get_response());
        return;
    }

    #path: /lokasi/list [GET]
    function get_lokasi_list(){
        #init variable
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/pembelian [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $operator           = $verify_resp['data'];
        $operator_lokasi    = $this->operator_lokasi_model->get_operator_lokasi_by_operator_id($operator->id);
        $lokasi_list        = array_map(function($item) {
            $lokasi = $this->lokasi_model->get_lokasi_by_id($item->lokasi_id);
            return $lokasi;
        }, $operator_lokasi);

        #response
        logging('debug', '/lokasi/list [GET] - Get lokasi list by operator success');
        $resp->set_response(200, "success", "Get lokasi list by operator success", $lokasi_list);
        set_output($resp->get_response());
        return;
    }

    #path: /lokasi/by-id/$id [GET]
    function get_lokasi_by_id($id){
        #check token
        $resp        = new Response_api();
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/lokasi/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get lokasi detail
        $lokasi = $this->lokasi_model->get_lokasi_by_id($id);
        if(is_null($lokasi)){
            logging('error', '/lokasi/by-id/'.$id.' [GET] - lokasi not found');
            $resp->set_response(404, "failed", "lokasi not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/lokasi/by-id/'.$id.' [GET] - Get lokasi by id success', $lokasi);
        $resp->set_response(200, "success", "Get lokasi by id success", $lokasi);
        set_output($resp->get_response());
        return;
    }

    #path: /lokasi [POST]
    function create_lokasi(){
        #check token
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/lokasi [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('lokasi');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/lokasi [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create lokasi
        $flag = $this->lokasi_model->create_lokasi($request);
        
        #response
        if(!$flag){
            logging('error', '/lokasi [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/lokasi [POST] - Create lokasi success', $request);
        $resp->set_response(200, "success", "Create lokasi success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /lokasi [PUT]
    function update_lokasi(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/lokasi [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'lokasi');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/lokasi [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check lokasi
        $lokasi = $this->lokasi_model->get_lokasi_by_id($request['id']);
        if(is_null($lokasi)){
            logging('error', '/lokasi [PUT] - lokasi not found', $request);
            $resp->set_response(404, "failed", "lokasi not found");
            set_output($resp->get_response());
            return;
        }

        #update lokasi
        $flag = $this->lokasi_model->update_lokasi($request);
        
        #response
        if(empty($flag)){
            logging('error', '/lokasi [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/lokasi [PUT] - Update lokasi success', $request);
        $resp->set_response(200, "success", "Update lokasi success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /lokasi/$id [DELETE]
    function delete_lokasi($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/lokasi/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check lokasi
        $lokasi = $this->lokasi_model->get_lokasi_by_id($id);
        if(is_null($lokasi)){
            logging('error', '/lokasi/'.$id.' [DELETE] - lokasi not found');
            $resp->set_response(404, "failed", "lokasi not found");
            set_output($resp->get_response());
            return;
        }

        #active lokasi
        $flag = $this->lokasi_model->delete_lokasi($id);
        
        #response
        if(empty($flag)){
            logging('error', '/lokasi/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/lokasi/'.$id.' [DELETE] - delete lokasi success');
        $resp->set_response(200, "success", "delete lokasi success");
        set_output($resp->get_response());
        return;
    }

}
