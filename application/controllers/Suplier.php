<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Suplier extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /suplier [GET]
    function get_suplier(){
        #check token
        $resp       = new Response_api();
        $header     = $this->input->request_headers();
        $verify_resp= verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/suplier/change-password [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/suplier [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get suplier
        $start          = $page_number * $page_size;
        $order          = array('field' => 'created_at', 'order' => 'DESC');
        if($order_by && $ordering){
            $order      = array('field' => $order_by, 'order' => $ordering);
        }
        $limit          = array('start' => $start, 'size' => $page_size);
        $suplier       = $this->suplier_model->get_suplier($search, $order, $limit);
        $records_total  = $this->suplier_model->count_suplier($search);

        if(empty($draw)){
            logging('debug', '/suplier [GET] - Get suplier is success', $suplier);
            $resp->set_response(200, "success", "Get suplier is success", $suplier);
            set_output($resp->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/suplier [GET] - Get suplier is success');
            $resp->set_response_datatable(200, $suplier, $draw, $records_total, $records_total);
            set_output($resp->get_response_datatable());
            return;
        } 
    }

    #path: /suplier/all [GET]
    function get_suplier_all(){
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/suplier/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get suplier detail
        $order    = array('field' => 'nama', 'order' => 'ASC');
        $suplier = $this->suplier_model->get_suplier(null, $order);

        #response
        logging('debug', '/suplier/all [GET] - Get suplier all success');
        $resp->set_response(200, "success", "Get suplier all success", $suplier);
        set_output($resp->get_response());
        return;
    }

    #path: /suplier/by-id/$id [GET]
    function get_suplier_by_id($id){
        #check token
        $resp        = new Response_api();
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/suplier/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get suplier detail
        $suplier = $this->suplier_model->get_suplier_by_id($id);
        if(is_null($suplier)){
            logging('error', '/suplier/by-id/'.$id.' [GET] - suplier not found');
            $resp->set_response(404, "failed", "suplier not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/suplier/by-id/'.$id.' [GET] - Get suplier by id success', $suplier);
        $resp->set_response(200, "success", "Get suplier by id success", $suplier);
        set_output($resp->get_response());
        return;
    }

    #path: /suplier [POST]
    function create_suplier(){
        #check token
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/suplier [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('nama', 'email', 'no_telpon');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/suplier [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create suplier
        $flag = $this->suplier_model->create_suplier($request);
        
        #response
        if(!$flag){
            logging('error', '/suplier [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/suplier [POST] - Create suplier success', $request);
        $resp->set_response(200, "success", "Create suplier success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /suplier [PUT]
    function update_suplier(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/suplier [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'nama', 'email', 'no_telpon');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/suplier [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check suplier
        $suplier = $this->suplier_model->get_suplier_by_id($request['id']);
        if(is_null($suplier)){
            logging('error', '/suplier [PUT] - suplier not found', $request);
            $resp->set_response(404, "failed", "suplier not found");
            set_output($resp->get_response());
            return;
        }

        #update suplier
        $flag = $this->suplier_model->update_suplier($request);
        
        #response
        if(empty($flag)){
            logging('error', '/suplier [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/suplier [PUT] - Update suplier success', $request);
        $resp->set_response(200, "success", "Update suplier success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /suplier/$id [DELETE]
    function delete_suplier($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/suplier/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check suplier
        $suplier = $this->suplier_model->get_suplier_by_id($id);
        if(is_null($suplier)){
            logging('error', '/suplier/'.$id.' [DELETE] - suplier not found');
            $resp->set_response(404, "failed", "suplier not found");
            set_output($resp->get_response());
            return;
        }

        #active suplier
        $flag = $this->suplier_model->delete_suplier($id);
        
        #response
        if(empty($flag)){
            logging('error', '/suplier/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/suplier/'.$id.' [DELETE] - delete suplier success');
        $resp->set_response(200, "success", "delete suplier success");
        set_output($resp->get_response());
        return;
    }

}
