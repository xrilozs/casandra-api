<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Stok extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /stok [GET]
    function get_stok(){
        #init variable
        $resp           = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $lokasi         = $this->input->get('lokasi');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        $params         = array($page_number, $page_size);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/stok [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $operator           = $verify_resp['data'];
        $operator_lokasi    = $this->operator_lokasi_model->get_operator_lokasi_by_operator_id($operator->id);
        if($lokasi){
            $find_lokasi = false;
            foreach ($operator_lokasi as $item) {
                if($item->lokasi_id == $lokasi){
                    $find_lokasi = true;
                    break;
                }
            }
            
            if($find_lokasi){
                $lokasi = [$lokasi];
            }else{
                logging('error', "/stok [GET] - invalid lokasi");
                $resp->set_response(400, "failed", "invalid lokasi");
                set_output($resp->get_response());
                return;
            }
        }else{
            $lokasi         = array_map(function($item) {
                return $item->lokasi_id;
            }, $operator_lokasi);
        }
        
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/stok [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get stok
        $start          = $page_number * $page_size;
        $order          = array('field'=>'created_at', 'order'=>'DESC');
        if($order_by && $ordering){
            $order = array('field'=>$order_by, 'order'=>$ordering);
        }
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $stok           = $this->stok_model->get_stok($search, $lokasi, $order, $limit);
        $query = $this->db->last_query();
        $records_total  = $this->stok_model->count_stok($search, $lokasi);
        
        #response
        if(empty($draw)){
          logging('debug', '/stok [GET] - Get stok is success', $stok);
          $resp->set_response(200, "success", "Get stok is success", $stok);
          set_output($resp->get_response());
          return;
        }else{
          logging('debug', '/stok [GET] - Get stok is success');
          $resp->set_response_datatable(200, $stok, $draw, $records_total, $records_total);
          set_output($resp->get_response_datatable());
          return;
        } 
    }

    #path: /stok/by-id/$id [GET]
    function get_stok_by_id($id){
        $resp        = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/stok/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get stok by id
        $stok = $this->stok_model->get_stok_by_id($id);
        if(is_null($stok)){
            logging('error', '/stok/by-id/'.$id.' [GET] - stok not found');
            $resp->set_response(404, "failed", "stok not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/stok/by-id/'.$id.' [GET] - Get stok by id success', $stok);
        $resp->set_response(200, "success", "Get stok by id success", $stok);
        set_output($resp->get_response());
        return;
    }
}