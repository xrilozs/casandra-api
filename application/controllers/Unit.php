<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Unit extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /unit [GET]
    function get_unit(){
        #check token
        $resp       = new Response_api();
        $header     = $this->input->request_headers();
        $verify_resp= verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/unit/change-password [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/unit [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get unit
        $start          = $page_number * $page_size;
        $order          = array('field' => 'created_at', 'order' => 'DESC');
        if($order_by && $ordering){
            $order      = array('field' => $order_by, 'order' => $ordering);
        }
        $limit          = array('start' => $start, 'size' => $page_size);
        $unit       = $this->unit_model->get_unit($search, $order, $limit);
        $records_total  = $this->unit_model->count_unit($search);

        if(empty($draw)){
            logging('debug', '/unit [GET] - Get unit is success', $unit);
            $resp->set_response(200, "success", "Get unit is success", $unit);
            set_output($resp->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/unit [GET] - Get unit is success');
            $resp->set_response_datatable(200, $unit, $draw, $records_total, $records_total);
            set_output($resp->get_response_datatable());
            return;
        } 
    }

    #path: /unit/all [GET]
    function get_unit_all(){
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/unit/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get unit detail
        $order    = array('field' => 'nama', 'order' => 'ASC');
        $unit = $this->unit_model->get_unit(null, $order);

        #response
        logging('debug', '/unit/all [GET] - Get unit all success');
        $resp->set_response(200, "success", "Get unit all success", $unit);
        set_output($resp->get_response());
        return;
    }

    #path: /unit/by-id/$id [GET]
    function get_unit_by_id($id){
        #check token
        $resp        = new Response_api();
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/unit/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get unit detail
        $unit = $this->unit_model->get_unit_by_id($id);
        if(is_null($unit)){
            logging('error', '/unit/by-id/'.$id.' [GET] - unit not found');
            $resp->set_response(404, "failed", "unit not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/unit/by-id/'.$id.' [GET] - Get unit by id success', $unit);
        $resp->set_response(200, "success", "Get unit by id success", $unit);
        set_output($resp->get_response());
        return;
    }

    #path: /unit [POST]
    function create_unit(){
        #check token
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/unit [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('nama', 'simbol');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/unit [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create unit
        $flag = $this->unit_model->create_unit($request);
        
        #response
        if(!$flag){
            logging('error', '/unit [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/unit [POST] - Create unit success', $request);
        $resp->set_response(200, "success", "Create unit success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /unit [PUT]
    function update_unit(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/unit [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'nama', 'simbol');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/unit [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check unit
        $unit = $this->unit_model->get_unit_by_id($request['id']);
        if(is_null($unit)){
            logging('error', '/unit [PUT] - unit not found', $request);
            $resp->set_response(404, "failed", "unit not found");
            set_output($resp->get_response());
            return;
        }

        #update unit
        $flag = $this->unit_model->update_unit($request);
        
        #response
        if(empty($flag)){
            logging('error', '/unit [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/unit [PUT] - Update unit success', $request);
        $resp->set_response(200, "success", "Update unit success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /unit/$id [DELETE]
    function delete_unit($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/unit/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check unit
        $unit = $this->unit_model->get_unit_by_id($id);
        if(is_null($unit)){
            logging('error', '/unit/'.$id.' [DELETE] - unit not found');
            $resp->set_response(404, "failed", "unit not found");
            set_output($resp->get_response());
            return;
        }

        #active unit
        $flag = $this->unit_model->delete_unit($id);
        
        #response
        if(empty($flag)){
            logging('error', '/unit/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/unit/'.$id.' [DELETE] - delete unit success');
        $resp->set_response(200, "success", "delete unit success");
        set_output($resp->get_response());
        return;
    }

}
