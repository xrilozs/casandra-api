<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Kategori extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /kategori [GET]
    function get_kategori(){
        #check token
        $resp       = new Response_api();
        $header     = $this->input->request_headers();
        $verify_resp= verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/kategori/change-password [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/kategori [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get kategori
        $start          = $page_number * $page_size;
        $order          = array('field' => 'created_at', 'order' => 'DESC');
        if($order_by && $ordering){
            $order      = array('field' => $order_by, 'order' => $ordering);
        }
        $limit          = array('start' => $start, 'size' => $page_size);
        $kategori       = $this->kategori_model->get_kategori($search, $order, $limit);
        $records_total  = $this->kategori_model->count_kategori($search);

        if(empty($draw)){
            logging('debug', '/kategori [GET] - Get kategori is success', $kategori);
            $resp->set_response(200, "success", "Get kategori is success", $kategori);
            set_output($resp->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/kategori [GET] - Get kategori is success');
            $resp->set_response_datatable(200, $kategori, $draw, $records_total, $records_total);
            set_output($resp->get_response_datatable());
            return;
        } 
    }

    #path: /kategori/all [GET]
    function get_kategori_all(){
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/kategori/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get kategori detail
        $order    = array('field' => 'kategori', 'order' => 'ASC');
        $kategori = $this->kategori_model->get_kategori(null, $order);

        #response
        logging('debug', '/kategori/all [GET] - Get kategori all success');
        $resp->set_response(200, "success", "Get kategori all success", $kategori);
        set_output($resp->get_response());
        return;
    }

    #path: /kategori/by-id/$id [GET]
    function get_kategori_by_id($id){
        #check token
        $resp        = new Response_api();
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/kategori/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get kategori detail
        $kategori = $this->kategori_model->get_kategori_by_id($id);
        if(is_null($kategori)){
            logging('error', '/kategori/by-id/'.$id.' [GET] - kategori not found');
            $resp->set_response(404, "failed", "kategori not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/kategori/by-id/'.$id.' [GET] - Get kategori by id success', $kategori);
        $resp->set_response(200, "success", "Get kategori by id success", $kategori);
        set_output($resp->get_response());
        return;
    }

    #path: /kategori [POST]
    function create_kategori(){
        #check token
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/kategori [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('kategori', 'kode_ginee');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/kategori [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check kategori exist
        $kategori_exist = $this->kategori_model->get_kategori_by_kode_ginee($request['kode_ginee']);
        if($kategori_exist){
            logging('error', '/kategori [POST] - Requested kategori already exist', $request);
            $resp->set_response(400, "failed", "Requested kategori already exist");
            set_output($resp->get_response());
            return;
        }

        #create kategori
        $flag = $this->kategori_model->create_kategori($request);
        
        #response
        if(!$flag){
            logging('error', '/kategori [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/kategori [POST] - Create kategori success', $request);
        $resp->set_response(200, "success", "Create kategori success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /kategori [PUT]
    function update_kategori(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/kategori [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'kategori', 'kode_ginee');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/kategori [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check kategori
        $kategori = $this->kategori_model->get_kategori_by_id($request['id']);
        if(is_null($kategori)){
            logging('error', '/kategori [PUT] - kategori not found', $request);
            $resp->set_response(404, "failed", "kategori not found");
            set_output($resp->get_response());
            return;
        }
        #check changes of kode ginee
        if($kategori->kode_ginee != $request['kode_ginee']){
            #check duplicate kode_ginee
            $kategori_exist = $this->kategori_model->get_kategori_by_kode_ginee($request['kode_ginee']);
            if($kategori_exist){
                logging('error', '/kategori [POST] - New kode ginee already registered', $request);
                $resp->set_response(400, "failed", "New kode ginee already registered");
                set_output($resp->get_response());
                return;
            }
        }

        #update kategori
        $flag = $this->kategori_model->update_kategori($request);
        
        #response
        if(empty($flag)){
            logging('error', '/kategori [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/kategori [PUT] - Update kategori success', $request);
        $resp->set_response(200, "success", "Update kategori success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /kategori/$id [DELETE]
    function delete_kategori($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/kategori/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check kategori
        $kategori = $this->kategori_model->get_kategori_by_id($id);
        if(is_null($kategori)){
            logging('error', '/kategori/'.$id.' [DELETE] - kategori not found');
            $resp->set_response(404, "failed", "kategori not found");
            set_output($resp->get_response());
            return;
        }

        #active kategori
        $flag = $this->kategori_model->delete_kategori($id);
        
        #response
        if(empty($flag)){
            logging('error', '/kategori/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/kategori/'.$id.' [DELETE] - delete kategori success');
        $resp->set_response(200, "success", "delete kategori success");
        set_output($resp->get_response());
        return;
    }

}
