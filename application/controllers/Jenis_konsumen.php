<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Jenis_konsumen extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /jenis-konsumen [GET]
    function get_jenis_konsumen(){
        #check token
        $resp       = new Response_api();
        $header     = $this->input->request_headers();
        $verify_resp= verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-konsumen [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/jenis-konsumen [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get jenis konsumen
        $start          = $page_number * $page_size;
        $order          = array('field' => 'created_at', 'order' => 'DESC');
        if($order_by && $ordering){
            $order      = array('field' => $order_by, 'order' => $ordering);
        }
        $limit          = array('start' => $start, 'size' => $page_size);
        $jenis_konsumen = $this->jenis_konsumen_model->get_jenis_konsumen($search, $order, $limit);
        $records_total  = $this->jenis_konsumen_model->count_jenis_konsumen($search);

        if(empty($draw)){
            logging('debug', '/jenis-konsumen [GET] - Get jenis konsumen is success', $jenis_konsumen);
            $resp->set_response(200, "success", "Get jenis konsumen is success", $jenis_konsumen);
            set_output($resp->get_response());
            return;
        }else{
            $output['draw'] = $draw;
            logging('debug', '/jenis-konsumen [GET] - Get jenis konsumen is success');
            $resp->set_response_datatable(200, $jenis_konsumen, $draw, $records_total, $records_total);
            set_output($resp->get_response_datatable());
            return;
        } 
    }

    #path: /jenis-konsumen/all [GET]
    function get_jenis_konsumen_all(){
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-konsumen/all [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get jenis konsumen detail
        $order          = array('field' => 'nama', 'order' => 'ASC');
        $jenis_konsumen = $this->jenis_konsumen_model->get_jenis_konsumen(null, $order);

        #response
        logging('debug', '/jenis-konsumen/all [GET] - Get jenis konsumen all success');
        $resp->set_response(200, "success", "Get jenis konsumen all success", $jenis_konsumen);
        set_output($resp->get_response());
        return;
    }

    #path: /jenis-konsumen/by-id/$id [GET]
    function get_jenis_konsumen_by_id($id){
        #check token
        $resp        = new Response_api();
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-konsumen/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get jenis konsumen detail
        $jenis_konsumen = $this->jenis_konsumen_model->get_jenis_konsumen_by_id($id);
        if(is_null($jenis_konsumen)){
            logging('error', '/jenis-konsumen/by-id/'.$id.' [GET] - jenis konsumen not found');
            $resp->set_response(404, "failed", "unit not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/jenis-konsumen/by-id/'.$id.' [GET] - Get jenis konsumen by id success', $jenis_konsumen);
        $resp->set_response(200, "success", "Get jenis konsumen by id success", $jenis_konsumen);
        set_output($resp->get_response());
        return;
    }

    #path: /jenis-konsumen [POST]
    function create_jenis_konsumen(){
        #check token
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-konsumen [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('nama', 'kode');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/jenis-konsumen [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create jenis konsumen
        $flag = $this->jenis_konsumen_model->create_jenis_konsumen($request);
        
        #response
        if(!$flag){
            logging('error', '/jenis-konsumen [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/jenis-konsumen [POST] - Create jenis konsumen success', $request);
        $resp->set_response(200, "success", "Create jenis konsumen success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /jenis-konsumen [PUT]
    function update_jenis_konsumen(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-konsumen [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'nama', 'kode');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/jenis-konsumen [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check jenis konsumen
        $jenis_konsumen = $this->jenis_konsumen_model->get_jenis_konsumen_by_id($request['id']);
        if(is_null($jenis_konsumen)){
            logging('error', '/jenis-konsumen [PUT] - jenis konsumen not found', $request);
            $resp->set_response(404, "failed", "unit not found");
            set_output($resp->get_response());
            return;
        }

        #update jenis konsumen
        $flag = $this->jenis_konsumen_model->update_jenis_konsumen($request);
        
        #response
        if(empty($flag)){
            logging('error', '/jenis-konsumen [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/jenis-konsumen [PUT] - Update jenis konsumen success', $request);
        $resp->set_response(200, "success", "Update jenis konsumen success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /jenis-konsumen/$id [DELETE]
    function delete_jenis_konsumen($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/jenis-konsumen/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check jenis konsumen
        $jenis_konsumen = $this->jenis_konsumen_model->get_jenis_konsumen_by_id($id);
        if(is_null($jenis_konsumen)){
            logging('error', '/jenis-konsumen/'.$id.' [DELETE] - jenis konsumen not found');
            $resp->set_response(404, "failed", "unit not found");
            set_output($resp->get_response());
            return;
        }

        #active jenis konsumen
        $flag = $this->jenis_konsumen_model->delete_jenis_konsumen($id);
        
        #response
        if(empty($flag)){
            logging('error', '/jenis-konsumen/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/jenis-konsumen/'.$id.' [DELETE] - delete jenis konsumen success');
        $resp->set_response(200, "success", "delete jenis konsumen success");
        set_output($resp->get_response());
        return;
    }

}
