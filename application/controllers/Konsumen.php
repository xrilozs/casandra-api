<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Konsumen extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /konsumen [GET]
    function get_konsumen(){
        #init variable
        $resp           = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        $params         = array($page_number, $page_size);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/konsumen [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/konsumen [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get konsumen
        $start          = $page_number * $page_size;
        $order          = array('field'=>'created_at', 'order'=>'DESC');
        if($order_by && $ordering){
            $order = array('field'=>$order_by, 'order'=>$ordering);
        }
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $konsumen       = $this->konsumen_model->get_konsumen($search, $order, $limit);
        $records_total  = $this->konsumen_model->count_konsumen($search);
        
        #response
        if(empty($draw)){
          logging('debug', '/konsumen [GET] - Get konsumen is success', $konsumen);
          $resp->set_response(200, "success", "Get konsumen is success", $konsumen);
          set_output($resp->get_response());
          return;
        }else{
          logging('debug', '/konsumen [GET] - Get konsumen is success');
          $resp->set_response_datatable(200, $konsumen, $draw, $records_total, $records_total);
          set_output($resp->get_response_datatable());
          return;
        } 
    }

    #path: /konsumen/all [GET]
    function get_konsumen_all(){
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/konsumen/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get konsumen detail
        $order      = array('field' => 'username', 'order' => 'ASC');
        $konsumen   = $this->konsumen_model->get_konsumen(null, $order);

        #response
        logging('debug', '/konsumen/all [GET] - Get konsumen all success');
        $resp->set_response(200, "success", "Get konsumen all success", $konsumen);
        set_output($resp->get_response());
        return;
    }

    #path: /konsumen/by-id/$id [GET]
    function get_konsumen_by_id($id){
        $resp        = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/konsumen/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get konsumen by id
        $konsumen = $this->konsumen_model->get_konsumen_by_id($id);
        if(is_null($konsumen)){
            logging('error', '/konsumen/by-id/'.$id.' [GET] - konsumen not found');
            $resp->set_response(404, "failed", "konsumen not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/konsumen/by-id/'.$id.' [GET] - Get konsumen by id success', $konsumen);
        $resp->set_response(200, "success", "Get konsumen by id success", $konsumen);
        set_output($resp->get_response());
        return;
    }
  
    #path: /konsumen [POST]
    function create_konsumen(){
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $allowed_role   = array('owner');
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/konsumen [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('username', 'no_telpon', 'email', 'dob', 'jenis_konsumen', 'no_member');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/konsumen [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        $flag = $this->konsumen_model->create_konsumen($request);
        #response
        if(!$flag){
            logging('error', '/konsumen [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/konsumen [POST] - Create konsumen success', $request);
        $resp->set_response(200, "success", "Create konsumen success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /konsumen [PUT]
    function update_konsumen(){
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $allowed_role   = array('owner');
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/konsumen [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'username', 'no_telpon', 'email', 'dob', 'jenis_konsumen', 'no_member');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/konsumen [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check konsumen exist
        $konsumen = $this->konsumen_model->get_konsumen_by_id($request['id']);
        if(is_null($konsumen)){
            logging('error', '/konsumen [PUT] - Konsumen not found');
            $resp->set_response(400, "failed", "Konsumen not found");
            set_output($resp->get_response());
            return;
        }

        $flag = $this->konsumen_model->update_konsumen($request);
        #response
        if(!$flag){
            logging('error', '/konsumen [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/konsumen [PUT] - Update konsumen success', $request);
        $resp->set_response(200, "success", "Update konsumen success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /konsumen/$id [DELETE]
    function delete_konsumen($id){
        #check token
        $resp           = new Response_api();
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/konsumen/'.$id.' [DELETE] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check konsumen
        $konsumen = $this->konsumen_model->get_konsumen_by_id($id);
        if(is_null($konsumen)){
            logging('error', '/konsumen/'.$id.' [DELETE] - konsumen not found');
            $resp->set_response(404, "failed", "konsumen not found");
            set_output($resp->get_response());
            return;
        }

        #delete konsumen
        $flag = $this->konsumen_model->delete_konsumen($id);
        
        #response
        if(empty($flag)){
            logging('error', '/konsumen/'.$id.' [DELETE] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/konsumen/'.$id.' [DELETE] - delete konsumen success');
        $resp->set_response(200, "success", "delete konsumen success");
        set_output($resp->get_response());
        return;
    }
}