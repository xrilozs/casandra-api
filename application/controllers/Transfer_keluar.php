<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

require_once('./vendor/autoload.php');
use Firebase\JWT\JWT;

class Transfer_keluar extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /transfer/keluar [GET]
    function get_transfer_keluar(){
        #init variable
        $resp           = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $kode_barang    = $this->input->get('kode_barang');
        $order_by       = $this->input->get('order_by');
        $ordering       = $this->input->get('ordering');
        $draw           = $this->input->get('draw');
        $params         = array($page_number, $page_size);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/transfer/keluar [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $operator           = $verify_resp['data'];
        $operator_lokasi    = $this->operator_lokasi_model->get_operator_lokasi_by_operator_id($operator->id);
        $lokasi             = array_map(function($item) {
            return $item->lokasi_id;
        }, $operator_lokasi);
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/transfer/keluar [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get transfer_keluar
        $start          = $page_number * $page_size;
        $order          = array('field'=>'created_at', 'order'=>'DESC');
        if($order_by && $ordering){
            $order = array('field'=>$order_by, 'order'=>$ordering);
        }
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $transfer_keluar = $this->transfer_keluar_model->get_transfer_keluar($search, $lokasi, $kode_barang, $order, $limit);
        $records_total  = $this->transfer_keluar_model->count_transfer_keluar($search, $lokasi, $kode_barang);
        
        #response
        if(empty($draw)){
          logging('debug', '/transfer/keluar [GET] - Get transfer keluar is success', $transfer_keluar);
          $resp->set_response(200, "success", "Get transfer keluar is success", $transfer_keluar);
          set_output($resp->get_response());
          return;
        }else{
          logging('debug', '/transfer/keluar [GET] - Get transfer keluar is success');
          $resp->set_response_datatable(200, $transfer_keluar, $draw, $records_total, $records_total);
          set_output($resp->get_response_datatable());
          return;
        } 
    }

    #path: /transfer/keluar/by-id/$id [GET]
    function get_transfer_keluar_by_id($id){
        $resp        = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_operator_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/transfer/keluar/by-id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get transfer_keluar by id
        $transfer_keluar = $this->transfer_keluar_model->get_transfer_keluar_by_id($id);
        if(is_null($transfer_keluar)){
            logging('error', '/transfer/keluar/by-id/'.$id.' [GET] - transfer keluar not found');
            $resp->set_response(404, "failed", "transfer keluar not found");
            set_output($resp->get_response());
            return;
        }

        #response
        logging('debug', '/transfer/keluar/by-id/'.$id.' [GET] - Get transfer keluar by id success', $transfer_keluar);
        $resp->set_response(200, "success", "Get transfer keluar by id success", $transfer_keluar);
        set_output($resp->get_response());
        return;
    }
  
    #path: /transfer/keluar [POST]
    function create_transfer_keluar(){
        $resp           = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $allowed_role   = array('owner');
        
        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_operator_token($header, $allowed_role);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/transfer/keluar [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $operator = $verify_resp['data'];
        
        #check request params
        $keys = array('no_faktur', 'kode_barang', 'lokasi_id', 'qty');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/transfer/keluar [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check lokasi
        $list_lokasi        = $this->operator_lokasi_model->get_operator_lokasi_by_operator_id($operator->id);
        $operator_lokasi_id = array_map(function($item) {
            return $item->lokasi_id;
        }, $list_lokasi);
        if(!in_array($request['lokasi_id'], $operator_lokasi_id)){
            logging('error', '/api/transfer/keluar [POST] - Invalid Lokasi', $request);
            $resp_obj->set_response(400, "failed", "Invalid Lokasi");
            set_output($resp_obj->get_response());
            return;
        }

        $this->db->trans_begin();
        try{
            #create transfer_keluar
            $request['id']  = get_uniq_id();
            $flag = $this->transfer_keluar_model->create_transfer_keluar($request);
            
            #response
            if(!$flag){
                $this->db->trans_rollback();
                logging('error', '/transfer/keluar [POST] - Internal server error', $request);
                $resp->set_response(500, "failed", "Internal server error");
                set_output($resp->get_response());
                return;
            }
            #create/update stock item
            $stock = $this->stok_model->get_stok_by_kode_and_lokasi($request['kode_barang'], $request['lokasi_id']);
            if($stock){
                $stok = array(
                    "id"    => $stock->id,
                    "stok"  => intval($stock->stok) - intval($request['qty'])
                );
                $flag = $this->stok_model->update_stok($stok);
                if(!$flag){
                    $this->db->trans_rollback();
                    logging('debug', '/transfer/keluar [POST] - Create transfer keluar failed. Update stock failed!');
                    $resp->set_response(500, "failed", "Internal server error");
                    set_output($resp->get_response());
                    return;
                }
            }else{
                $this->db->trans_rollback();
                logging('debug', '/transfer/keluar [POST] - Create transfer keluar failed. Stok is not found!');
                $resp->set_response(500, "failed", "Internal server error");
                set_output($resp->get_response());
                return;
            }
        }catch(Exception $e){
            $this->db->trans_rollback();
            logging('debug', '/transfer/keluar [POST] - Create transfer keluar failed', $e);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        $this->db->trans_commit();

        logging('debug', '/transfer/keluar [POST] - Create transfer keluar success', $request);
        $resp->set_response(200, "success", "Create transfer keluar success", $request);
        set_output($resp->get_response());
        return;
    }
}