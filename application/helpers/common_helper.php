<?php
  function check_parameter($params){
    foreach($params as $param){
      if(is_null($param)){
        return false;
      }
    }
    return true;
  }

  function check_parameter_by_keys($data, $keys){
    foreach($keys as $key){
      if(!array_key_exists($key, $data)){
        return false;
      }
    }
    return true;
  }

  function get_uniq_id(){
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
      mt_rand( 0, 0xffff ),
      mt_rand( 0, 0x0C2f ) | 0x4000,
      mt_rand( 0, 0x3fff ) | 0x8000,
      mt_rand( 0, 0x2Aff ), mt_rand( 0, 0xffD3 ), mt_rand( 0, 0xff4B )
    );
  }

  function get_affiliate_code(){
    $randomNum = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 7);
    return strtoupper($randomNum);
  }

  function get_unique_code(){
    $randomNum = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 3);
    return strtoupper($randomNum);
  }

  function generate_alias($str){
    $str = preg_replace('/[^a-z0-9\s\-]/i', '', $str);
    // Replace all spaces with hyphens
    $str = preg_replace('/\s/', '-', $str);
    // Replace multiple hyphens with a single hyphen
    $str = preg_replace('/\-\-+/', '-', $str);
    // Remove leading and trailing hyphens, and then lowercase the URL
    $str = strtolower(trim($str, '-'));

    return $str;
  }

  function set_output($resp){
    $CI =& get_instance();
    $CI->output
        ->set_header('Access-Control-Allow-Origin: *')
        ->set_status_header($resp['code'])
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($resp));
  }

  function logging($type, $message, $data=null){
    log_message($type, $message);
    if($data){
      log_message($type, 'Request/Response: '.json_encode($data));
    }
  }
?>
