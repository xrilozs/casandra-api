<?php
    function resize_image($path, $filename){
        $CI =& get_instance();
        $source_path = $path . $filename;
        $target_path = $path . 'thumbnail/' . $filename;
        $config = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'width' => 360
        );
        $CI->load->library('image_lib');
        $CI->image_lib->initialize($config);

        if (!$CI->image_lib->resize()) {
            echo $CI->image_lib->display_errors();
            return array("status"=>"failed", "message"=>$CI->image_lib->display_errors());
        }
        $CI->image_lib->clear();

        return array("status"=>"success", "data"=>$target_path);
    }
?>