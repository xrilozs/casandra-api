<?php
  require 'vendor/autoload.php';
  use Firebase\JWT\JWT;

  function verify_operator_token($header, $allowed_role=null){
    $CI       =& get_instance();
    $resp_obj = new Response_api();
    
    #check header
    if(isset($header['Authorization'])){
      list(, $token) = explode(' ', $header['Authorization']);
    }else{
      $resp_obj->set_response(401, "failed", "Please use token to access this resource.");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    #check token
    try {
      $jwt = JWT::decode($token, ACCESS_TOKEN_SECRET, ['HS256']);
    } catch (Exception $e) {
      $resp_obj->set_response(401, "failed", "Invalid requested token");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    $username = $jwt->username;
    $operator = $CI->operator_model->get_operator_by_username($username);
    
    #check Operator exist
    if(is_null($operator)){
      $resp_obj->set_response(401, "failed", "Operator not found");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    #check allowed role
    if($allowed_role){
      if(!in_array($operator->role, $allowed_role)){
        $resp_obj->set_response(401, "failed", "Unauthorized role access");
        $resp = $resp_obj->get_response();
        return $resp;
      }
    }
    
    $resp_obj->set_response(200, "success", "Authorized access", $operator);
    $resp = $resp_obj->get_response();
    return $resp;
  }

?>