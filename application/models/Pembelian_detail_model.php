<?php
  class Pembelian_detail_model extends CI_Model{
    public $id;
    public $no_faktur;
    public $kode_barang;
    public $harga;
    public $qty;
    public $diskon;
    public $ppn;
    public $harga_pokok;

    function get_pembelian_detail($search=null, $date=null, $lokasi=null, $order=null, $limit=null){
      $this->db->select("pd.*, p.tanggal, l.lokasi, s.nama as nama_suplier, i.nama_barang");
      if($search){
        $where_search = "(
          pd.no_faktur LIKE '%$search%' OR
          pd.kode_barang LIKE '%$search%' OR
          pd.harga LIKE '%$search%' OR
          pd.qty LIKE '%$search%' OR
          pd.diskon LIKE '%$search%' OR
          p.tanggal LIKE '%$search%' OR
          l.lokasi LIKE '%$search%' OR
          s.nama LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($date){
        $this->db->where("p.tanggal", $date);
      }
      if($lokasi){
        $this->db->where_in("p.lokasi_id", $lokasi);
      }
      if($order){
        $table_alias = "pd";
        if($order['field'] == 'nama_barang'){
          $table_alias = "i";
        }else if($order['field'] == 'lokasi'){
          $table_alias = "l";
        }else if($order['field'] == 'nama_suplier'){
          $table_alias = "s";
          $order['field'] = 'nama';
        }
        $this->db->order_by("$table_alias.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->join("pembelian p", "p.no_faktur = pd.no_faktur");
      $this->db->join("lokasi l", "l.id = p.lokasi_id");
      $this->db->join("suplier s", "s.id_suplier = p.suplier_id");
      $this->db->join("item i", "i.kode = pd.kode_barang");
      $query = $this->db->get('pembelian_detail pd');
      return $query->result();
    }

    function count_pembelian_detail($search=null, $date=null, $lokasi=null){
      if($search){
        $where_search = "(
          pd.no_faktur LIKE '%$search%' OR
          pd.kode_barang LIKE '%$search%' OR
          pd.harga LIKE '%$search%' OR
          pd.qty LIKE '%$search%' OR
          pd.diskon LIKE '%$search%' OR
          p.tanggal LIKE '%$search%' OR
          l.lokasi LIKE '%$search%' OR
          s.nama LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($date){
        $this->db->where("p.tanggal", $date);
      }
      if($lokasi){
        $this->db->where_in("p.lokasi_id", $lokasi);
      }
      $this->db->join("pembelian p", "p.no_faktur = pd.no_faktur");
      $this->db->join("lokasi l", "l.id = p.lokasi_id");
      $this->db->join("suplier s", "s.id_suplier = p.suplier_id");
      $query = $this->db->get('pembelian_detail pd');
      return $this->db->count_all_results();
    }

    function get_pembelian_detail_by_id($id){
      $this->db->select("pd.*, p.tanggal, l.lokasi, s.nama as nama_suplier, i.nama_barang");
      $this->db->where("pd.id", $id);
      $this->db->join("pembelian p", "p.no_faktur = pd.no_faktur");
      $this->db->join("lokasi l", "l.id = p.lokasi_id");
      $this->db->join("suplier s", "s.id_suplier = p.suplier_id");
      $this->db->join("item i", "i.kode = pd.kode_barang");
      $query = $this->db->get('pembelian_detail pd');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_rata_beli_by_kode_and_lokasi($kode, $lokasi){
      $this->db->select("AVG(pd.harga_pokok) as rata_beli");
      $this->db->where("pd.kode_barang", $kode);
      $this->db->where("p.lokasi_id", $lokasi);
      $this->db->join("pembelian p", "p.no_faktur = pd.no_faktur");
      $query = $this->db->get('pembelian_detail pd');
      return $query->row();
    }

    function create_pembelian_detail($data){
      $this->db->insert('pembelian_detail', $data);
      return $this->db->affected_rows();
    }

  }
?>
