<?php
  class Transfer_masuk_model extends CI_Model{
    public $id;
    public $no_faktur;
    public $kode_barang;
    public $tanggal;
    public $qty;
    public $lokasi_id;

    function get_transfer_masuk($search=null, $lokasi=null, $kode_barang=null, $order=null, $limit=null){
      $this->db->select("t.*, l.lokasi, i.nama_barang");
      if($search){
        $where_search = "(
          t.no_faktur LIKE '%$search%' OR
          t.kode_barang LIKE '%$search%' OR
          l.lokasi LIKE '%$search%' OR
          i.nama_barang LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($lokasi){
        $this->db->where_in("t.lokasi_id", $lokasi);
      }
      if($kode_barang){
        $this->db->where("t.kode_barang", $kode_barang);
      }
      if($order){
        $table_alias = "t";
        if($order['field'] == 'nama_barang'){
          $table_alias = "i";
        }else if($order['field'] == 'lokasi'){
          $table_alias = "l";
        }
        $this->db->order_by("$table_alias.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->join("lokasi l", "l.id = t.lokasi_id");
      $this->db->join("item i", "i.kode = t.kode_barang");
      $query = $this->db->get('transfer_masuk t');
      return $query->result();
    }

    function count_transfer_masuk($search=null, $lokasi=null, $kode_barang=null){
      if($search){
        $where_search = "(
          t.no_faktur LIKE '%$search%' OR
          t.kode_barang LIKE '%$search%' OR
          l.lokasi LIKE '%$search%' OR
          i.nama_barang LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($lokasi){
        $this->db->where_in("t.lokasi_id", $lokasi);
      }
      if($kode_barang){
        $this->db->where("t.kode_barang", $kode_barang);
      }
      $this->db->join("lokasi l", "l.id = t.lokasi_id");
      $this->db->join("item i", "i.kode = t.kode_barang");
      $this->db->get('transfer_masuk t');
      return $this->db->count_all_results();
    }

    function get_transfer_masuk_by_id($id){
      $this->db->select("t.*, l.lokasi, i.nama_barang");
      $this->db->where("t.id", $id);
      $this->db->join("lokasi l", "l.id = t.lokasi_id");
      $this->db->join("item i", "i.kode = t.kode_barang");
      $query = $this->db->get('transfer_masuk t');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_transfer_masuk($data){
      $this->id           = $data['id'];
      $this->no_faktur    = $data['no_faktur'];
      $this->kode_barang  = $data['kode_barang'];
      $this->qty          = $data['qty'];
      $this->lokasi_id    = $data['lokasi_id'];
      $this->tanggal      = date('Y-m-d');
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert('transfer_masuk', $this);
      return $this->db->affected_rows();
    }
  }
?>
