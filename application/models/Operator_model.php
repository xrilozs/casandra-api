<?php
  class Operator_model extends CI_Model{
    public $username;
    public $password;
    public $role;
    public $hak_akses;
    
    function get_operator_by_username($username){
      $this->db->where("username", $username);
      $query = $this->db->get('operator');
      return $query->num_rows() ? $query->row() : null;
    }

    function get_operators($search=null, $role=null, $order=null, $limit=null){
      $this->db->select("o.*");
      if($search){
        $where_search = "(
          o.username LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($role){
        $this->db->where('o.role', $role);
      }
      $this->db->from('operator as o');
      if($order){
        $this->db->order_by("o.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_operator($search=null, $role=null){
      $this->db->from('operator');
      if($search){
        $where_search = "(
          username LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($role){
        $this->db->where("role", $role);
      }
      return $this->db->count_all_results();
    }

    function get_operator_by_id($id){
      $this->db->where("id", $id);
      $this->db->from('operator');
      $query = $this->db->get();
      return $query->num_rows() ? $query->row() : null;
    }

    function create_operator($data){
      $this->username   = $data['username'];
      $this->password   = $data['password'];
      $this->role       = $data['role'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('operator', $this);
      return $this->db->insert_id();
    }

    function update_operator($data){
      $this->username = $data['username'];
      $this->password = $data['password'];
      $this->role     = $data['role'];

      $this->db->update('operator', $this, array("id"=>$data['id']));
      return $this->db->affected_rows();
    }

    function delete_operator($id){
      $this->db->where('id', $id);
      $this->db->delete('operator');
      return $this->db->affected_rows();
    }
    
    function change_password($id, $password){
      $req = array(
        'password' => $password
      );
      $this->db->update('operator', $req, array('id'=>$id));
      return $this->db->affected_rows();
    }
  }
?>
