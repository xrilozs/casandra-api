<?php
  class Lokasi_model extends CI_Model{
    public $lokasi;

    function get_lokasi($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          lokasi LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('lokasi');
      return $query->result();
    }

    function count_lokasi($search=null){
      if($search){
        $where_search = "(
          lokasi LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->from('lokasi');
      return $this->db->count_all_results();
    }

    function get_lokasi_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('lokasi');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_lokasi($data){
      $this->lokasi     = $data['lokasi'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('lokasi', $this);
      return $this->db->affected_rows();
    }

    function update_lokasi($data){
      $this->lokasi   = $data['lokasi'];

      $this->db->update('lokasi', $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_lokasi($id){
      $this->db->where('id', $id);
      $this->db->delete('lokasi');
      return $this->db->affected_rows();
    }

  }
?>
