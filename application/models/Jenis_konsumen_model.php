<?php
  class Jenis_konsumen_model extends CI_Model{
    public $kode;
    public $nama;

    function get_jenis_konsumen($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          nama LIKE '%$search%' OR
          kode LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('jenis_konsumen');
      return $query->result();
    }

    function count_jenis_konsumen($search=null){
      if($search){
        $where_search = "(
          nama LIKE '%$search%' OR
          kode LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->from('jenis_konsumen');
      return $this->db->count_all_results();
    }

    function get_jenis_konsumen_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('jenis_konsumen');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_jenis_konsumen($data){
      $this->nama       = $data['nama'];
      $this->kode       = $data['kode'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('jenis_konsumen', $this);
      return $this->db->affected_rows();
    }

    function update_jenis_konsumen($data){
      $this->nama       = $data['nama'];
      $this->kode       = $data['kode'];

      $this->db->update('jenis_konsumen', $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_jenis_konsumen($id){
      $this->db->where('id', $id);
      $this->db->delete('jenis_konsumen');
      return $this->db->affected_rows();
    }

  }
?>
