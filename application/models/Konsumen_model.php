<?php
  class Konsumen_model extends CI_Model{
    public $username;
    public $no_telpon;
    public $email;
    public $dob;
    public $jenis_konsumen;
    public $no_member;

    function get_konsumen($search=null, $order=null, $limit=null){
      $this->db->select("k.*, jk.nama as jenis_konsumen_nama");
      if($search){
        $where_search = "(
          t.username LIKE '%$search%' OR
          t.no_telpon LIKE '%$search%' OR
          l.email LIKE '%$search%' OR
          i.dob LIKE '%$search%' OR
          i.jenis_konsumen LIKE '%$search%' OR
          jk.nama LIKE '%$search%' OR
          i.no_member LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($order){
        $table_alias = "k";
        if($order['field'] == 'jenis_konsumen_nama'){
          $table_alias    = "jk";
          $order['field'] = "nama";
        }
        $this->db->order_by("$table_alias.".$order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->join("jenis_konsumen jk", "jk.kode = k.jenis_konsumen");
      $query = $this->db->get('konsumen k');
      return $query->result();
    }

    function count_konsumen($search=null){
      if($search){
        $where_search = "(
          t.username LIKE '%$search%' OR
          t.no_telpon LIKE '%$search%' OR
          l.email LIKE '%$search%' OR
          i.dob LIKE '%$search%' OR
          i.jenis_konsumen LIKE '%$search%' OR
          jk.nama LIKE '%$search%' OR
          i.no_member LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->join("jenis_konsumen jk", "jk.kode = k.jenis_konsumen");
      $this->db->get('konsumen k');
      return $this->db->count_all_results();
    }

    function get_konsumen_by_id($id){
      $this->db->select("k.*, jk.nama as jenis_konsumen_nama");
      $this->db->where("k.id", $id);
      $this->db->join("jenis_konsumen jk", "jk.kode = k.jenis_konsumen");
      $query = $this->db->get('konsumen k');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_konsumen($data){
      $this->username       = $data['username'];
      $this->no_telpon      = $data['no_telpon'];
      $this->email          = $data['email'];
      $this->dob            = $data['dob'];
      $this->jenis_konsumen = $data['jenis_konsumen'];
      $this->no_member      = $data['no_member'];
      $this->created_at     = date('Y-m-d H:i:s');

      $this->db->insert('konsumen', $this);
      return $this->db->affected_rows();
    }

    function update_konsumen($data){
      $this->username       = $data['username'];
      $this->no_telpon      = $data['no_telpon'];
      $this->email          = $data['email'];
      $this->dob            = $data['dob'];
      $this->jenis_konsumen = $data['jenis_konsumen'];
      $this->no_member      = $data['no_member'];

      $this->db->update('konsumen', $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_konsumen($id){
      $this->db->where('id', $id);
      $this->db->delete('konsumen');
      return $this->db->affected_rows();
    }
  }
?>
