<?php
  class Variasi2_model extends CI_Model{
    public $ukuran;

    function get_variasi2($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          ukuran LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('variasi2');
      return $query->result();
    }

    function count_variasi2($search=null){
      if($search){
        $where_search = "(
          ukuran LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->from('variasi2');
      return $this->db->count_all_results();
    }

    function get_variasi2_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('variasi2');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_variasi2($data){
      $this->ukuran     = $data['ukuran'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('variasi2', $this);
      return $this->db->affected_rows();
    }

    function update_variasi2($data){
      $this->ukuran = $data['ukuran'];

      $this->db->update('variasi2', $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_variasi2($id){
      $this->db->where('id', $id);
      $this->db->delete('variasi2');
      return $this->db->affected_rows();
    }

  }
?>
