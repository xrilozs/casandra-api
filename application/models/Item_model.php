<?php
  class Item_model extends CI_Model{
    public $id;
    public $kode;
    public $kode_master;
    public $model;
    public $nama_barang;
    public $kode_satuan;
    public $kode_kategori;
    public $id_suplier;
    public $kode_variasi1;
    public $kode_variasi2;
    public $keterangan;
    public $status;

    function get_item($search=null, $type=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          kode LIKE '%$search%' OR
          kode_master LIKE '%$search%' OR
          model LIKE '%$search%' OR
          nama_barang LIKE '%$search%' OR
          keterangan LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($type){
        $this->db->where("status", $type);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('item');
      return $query->result();
    }

    function count_item($search=null, $type=null){
      if($search){
        $where_search = "(
          kode LIKE '%$search%' OR
          kode_master LIKE '%$search%' OR
          model LIKE '%$search%' OR
          nama_barang LIKE '%$search%' OR
          keterangan LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($type){
        $this->db->where("status", $type);
      }
      $this->db->from('item');
      return $this->db->count_all_results();
    }

    function get_item_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('item');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_item_by_kode($kode){
      $this->db->where("kode", $kode);
      $query = $this->db->get('item');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_item_by_kode_master($kode_master){
      $this->db->where("kode_master", $kode_master);
      $query = $this->db->get('item');
      return $query->result();
    }

    function create_item($data){
      $this->id             = $data['id'];
      $this->kode           = $data['kode'];
      $this->kode_master    = array_key_exists("kode_master", $data) ? $data['kode_master'] : null;
      $this->model          = $data['model'];
      $this->nama_barang    = $data['nama_barang'];
      $this->kode_satuan    = array_key_exists("kode_satuan", $data) ? $data['kode_satuan'] : null;
      $this->kode_kategori  = array_key_exists("kode_kategori", $data) ? $data['kode_kategori'] : null;
      $this->id_suplier     = array_key_exists("id_suplier", $data) ? $data['id_suplier'] : null;
      $this->kode_variasi1  = array_key_exists("kode_variasi1", $data) ? $data['kode_variasi1'] : null;
      $this->kode_variasi2  = array_key_exists("kode_variasi2", $data) ? $data['kode_variasi2'] : null;
      $this->keterangan     = array_key_exists("keterangan", $data) ? $data['keterangan'] : null;
      $this->status         = $data['status'];
      $this->created_at     = date('Y-m-d H:i:s');

      $this->db->insert('item', $this);
      return $this->db->affected_rows();
    }

    function update_item($data){
      $this->id             = $data['id'];
      $this->kode           = $data['kode'];
      $this->kode_master    = array_key_exists("kode_master", $data) ? $data['kode_master'] : null;
      $this->model          = $data['model'];
      $this->nama_barang    = $data['nama_barang'];
      $this->kode_satuan    = array_key_exists("kode_satuan", $data) ? $data['kode_satuan'] : null;
      $this->kode_kategori  = array_key_exists("kode_kategori", $data) ? $data['kode_kategori'] : null;
      $this->id_suplier     = array_key_exists("id_suplier", $data) ? $data['id_suplier'] : null;
      $this->kode_variasi1  = array_key_exists("kode_variasi1", $data) ? $data['kode_variasi1'] : null;
      $this->kode_variasi2  = array_key_exists("kode_variasi2", $data) ? $data['kode_variasi2'] : null;
      $this->keterangan     = array_key_exists("keterangan", $data) ? $data['keterangan'] : null;
      $this->status         = $data['status'];

      $this->db->update('item', $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_item($id){
      $this->db->where('id', $id);
      $this->db->delete('item');
      return $this->db->affected_rows();
    }

  }
?>
