<?php
  class Operator_lokasi_model extends CI_Model{
    public $lokasi_id;
    public $operator_id;


    function get_operator_lokasi_by_id($id){
      $this->db->select("ol.*, o.username, l.lokasi");
      $this->db->where("ol.id", $id);
      $this->db->join("operator o", "o.id = ol.operator_id");
      $this->db->join("lokasi l", "l.id = ol.lokasi_id");
      $query = $this->db->get('operator_lokasi ol');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_operator_lokasi_by_operator_id($operator_id){
      $this->db->select("ol.*, o.username, l.lokasi");
      $this->db->where("ol.operator_id", $operator_id);
      $this->db->join("operator o", "o.id = ol.operator_id");
      $this->db->join("lokasi l", "l.id = ol.lokasi_id");
      $query = $this->db->get('operator_lokasi ol');
      return $query->result();
    }

    function get_operator_lokasi_by_operator_id_and_lokasi_id($operator_id, $lokasi_id){
      $this->db->select("ol.*, o.username, l.lokasi");
      $this->db->where("ol.operator_id", $operator_id);
      $this->db->where("ol.lokasi_id", $lokasi_id);
      $this->db->join("operator o", "o.id = ol.operator_id");
      $this->db->join("lokasi l", "l.id = ol.lokasi_id");
      $query = $this->db->get('operator_lokasi ol');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_operator_lokasi($data){
      $this->lokasi_id  = $data['lokasi_id'];
      $this->operator_id= $data['operator_id'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('operator_lokasi', $this);
      return $this->db->affected_rows();
    }

    function delete_operator_lokasi($id){
      $this->db->where('id', $id);
      $this->db->delete('operator_lokasi');
      return $this->db->affected_rows();
    }

    function delete_operator_lokasi2($operator_id, $lokasi_id){
      $this->db->where('operator_id', $operator_id);
      $this->db->where('lokasi_id', $lokasi_id);
      $this->db->delete('operator_lokasi');
      return $this->db->affected_rows();
    }

  }
?>
