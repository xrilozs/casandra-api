<?php
  class Kategori_model extends CI_Model{
    public $kategori;
    public $kode_ginee;
    public $diskon;

    function get_kategori($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          kategori LIKE '%$search%' OR
          kode_ginee LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('kategori');
      return $query->result();
    }

    function count_kategori($search=null){
      if($search){
        $where_search = "(
          kategori LIKE '%$search%' OR
          kode_ginee LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->from('kategori');
      return $this->db->count_all_results();
    }

    function get_kategori_by_id($id){
      $this->db->where("kode", $id);
      $query = $this->db->get('kategori');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_kategori_by_kode_ginee($kode_ginee){
      $this->db->where("kode_ginee", $kode_ginee);
      $query = $this->db->get('kategori');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_kategori($data){
      $this->kategori   = $data['kategori'];
      $this->kode_ginee = $data['kode_ginee'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('kategori', $this);
      return $this->db->affected_rows();
    }

    function update_kategori($data){
      $this->kategori   = $data['kategori'];
      $this->kode_ginee = $data['kode_ginee'];

      $this->db->update('kategori', $this, array('kode' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_kategori($id){
      $this->db->where('kode', $id);
      $this->db->delete('kategori');
      return $this->db->affected_rows();
    }

  }
?>
