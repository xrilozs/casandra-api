<?php
  class Stok_model extends CI_Model{
    public $id;
    public $lokasi_id;
    public $kode_barang;
    public $stok;
    public $rata_beli;
    public $harga_beli;
    public $harga_jual;
    public $harga_grosir;
    public $harga_pokok;
    public $harga_retail;
    public $harga_reseller;
    public $opname;
    public $acc;

    function get_stok($search=null, $lokasi=null, $order=null, $limit=null){
      $this->db->select("s.*, i.kode_master, i.nama_barang, l.lokasi, k.kategori, u.nama as unit, su.nama as suplier, v1.warna, v2.ukuran");
      if($search){
        $where_search = "(
          i.kode LIKE '%$search%' OR
          i.kode_master LIKE '%$search%' OR
          i.nama_barang LIKE '%$search%' OR
          l.lokasi LIKE '%$search%' OR
          k.kategori LIKE '%$search%' OR
          u.nama LIKE '%$search%' OR
          su.nama LIKE '%$search%' OR
          v1.warna LIKE '%$search%' OR
          v2.ukuran LIKE '%$search%' OR
          s.stok LIKE '%$search%' OR
          s.rata_beli LIKE '%$search%' OR
          s.harga_beli LIKE '%$search%' OR
          s.harga_jual LIKE '%$search%' OR
          s.harga_grosir LIKE '%$search%' OR
          s.harga_pokok LIKE '%$search%' OR
          s.harga_retail LIKE '%$search%' OR
          s.harga_reseller LIKE '%$search%' OR
        )";
        $this->db->where($where_search);
      }
      if($lokasi){
        $this->db->where_in("s.lokasi_id", $lokasi);
      }
      if($order){
        $table_alias = "s";
        $field = $order['field'];
        if(in_array($field, array("kode_master", "nama_barang"))){
          $table_alias = "i";
        }else if($field == 'lokasi'){
          $table_alias = "l";
        }else if($field == 'kategori'){
          $table_alias = "k";
        }else if($field == 'warna'){
          $table_alias = "v1";
        }else if($field == 'ukuran'){
          $table_alias = "v2";
        }else if($field == 'unit'){
          $table_alias = "u";
          $field = "nama";
        }else if($field == 'suplier'){
          $table_alias = "su";
          $field = "nama";
        }
        $this->db->order_by("$table_alias.$field", $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->join("lokasi l", "l.id = s.lokasi_id", "LEFT");
      $this->db->join("item i", "i.kode = s.kode_barang", "LEFT");
      $this->db->join("item i2", "i2.kode = i.kode_master", "LEFT");
      $this->db->join("kategori k", "k.kode = i2.kode_kategori", "LEFT");
      $this->db->join("unit u", "u.kode = i2.kode_satuan", "LEFT");
      $this->db->join("suplier su", "su.id_suplier = i2.id_suplier", "LEFT");
      $this->db->join("variasi1 v1", "v1.id = i.kode_variasi1", "LEFT");
      $this->db->join("variasi2 v2", "v2.id = i.kode_variasi2", "LEFT");
      $query = $this->db->get('stok s');
      return $query->result();
    }

    function count_stok($search=null, $lokasi=null){
      if($search){
        $where_search = "(
          i.kode LIKE '%$search%' OR
          i.kode_master LIKE '%$search%' OR
          i.model LIKE '%$search%' OR
          i.nama_barang LIKE '%$search%' OR
          l.lokasi LIKE '%$search%' OR
          k.kategori LIKE '%$search%' OR
          u.nama LIKE '%$search%' OR
          su.nama LIKE '%$search%' OR
          v1.warna LIKE '%$search%' OR
          v2.ukuran LIKE '%$search%' OR
          s.stok LIKE '%$search%' OR
          s.rata_beli LIKE '%$search%' OR
          s.harga_beli LIKE '%$search%' OR
          s.harga_jual LIKE '%$search%' OR
          s.harga_grosir LIKE '%$search%' OR
          s.harga_pokok LIKE '%$search%' OR
          s.harga_retail LIKE '%$search%' OR
          s.harga_reseller LIKE '%$search%' OR
        )";
        $this->db->where($where_search);
      }
      if($lokasi){
        $this->db->where_in("s.lokasi_id", $lokasi);
      }
      $this->db->join("lokasi l", "l.id = s.lokasi_id", "LEFT");
      $this->db->join("item i", "i.kode = s.kode_barang", "LEFT");
      $this->db->join("item i2", "i2.kode = i.kode", "LEFT");
      $this->db->join("kategori k", "k.kode = i2.kode_kategori", "LEFT");
      $this->db->join("unit u", "u.kode = i2.kode_satuan", "LEFT");
      $this->db->join("suplier su", "su.id_suplier = i2.id_suplier", "LEFT");
      $this->db->join("variasi1 v1", "v1.id = i.kode_variasi1", "LEFT");
      $this->db->join("variasi2 v2", "v2.id = i.kode_variasi2", "LEFT");
      $this->db->from('stok s');
      return $this->db->count_all_results();
    }

    function get_stok_by_id($id){
      $this->db->select("s.*, i.kode_master, i.nama_barang, l.lokasi, k.kategori, u.nama as unit, su.nama as suplier, v1.warna, v2.ukuran");
      $this->db->where("s.id", $id);
      $this->db->join("lokasi l", "l.id = s.lokasi_id", "LEFT");
      $this->db->join("item i", "i.kode = s.kode_barang", "LEFT");
      $this->db->join("item i2", "i2.kode = i.kode_master", "LEFT");
      $this->db->join("kategori k", "k.kode = i2.kode_kategori", "LEFT");
      $this->db->join("unit u", "u.kode = i2.kode_satuan", "LEFT");
      $this->db->join("suplier su", "su.id_suplier = i2.id_suplier", "LEFT");
      $this->db->join("variasi1 v1", "v1.id = i.kode_variasi1", "LEFT");
      $this->db->join("variasi2 v2", "v2.id = i.kode_variasi2", "LEFT");
      $query = $this->db->get('stok s');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_stok_by_kode_and_lokasi($kode, $lokasi){
      $this->db->where("kode_barang", $kode);
      $this->db->where("lokasi_id", $lokasi);
      $query = $this->db->get('stok');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_stok($data){
      $this->db->insert('stok', $data);
      return $this->db->affected_rows();
    }

    function update_stok($data){
      $this->db->update('stok', $data, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

  }
?>
