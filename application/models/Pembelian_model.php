<?php
  class Pembelian_model extends CI_Model{
    public $id;
    public $no_faktur;
    public $tanggal;
    public $operator_id;
    public $suplier_id;
    public $lokasi_id;
    public $keterangan;

    function create_pembelian($data){
      $this->db->insert('pembelian', $data);
      return $this->db->affected_rows();
    }

  }
?>
