<?php
  class Unit_model extends CI_Model{
    public $nama;
    public $simbol;

    function get_unit($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          nama LIKE '%$search%' OR
          simbol LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('unit');
      return $query->result();
    }

    function count_unit($search=null){
      if($search){
        $where_search = "(
          nama LIKE '%$search%' OR
          simbol LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->from('unit');
      return $this->db->count_all_results();
    }

    function get_unit_by_id($id){
      $this->db->where("kode", $id);
      $query = $this->db->get('unit');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_unit($data){
      $this->nama       = $data['nama'];
      $this->simbol     = $data['simbol'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('unit', $this);
      return $this->db->affected_rows();
    }

    function update_unit($data){
      $this->nama       = $data['nama'];
      $this->simbol     = $data['simbol'];

      $this->db->update('unit', $this, array('kode' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_unit($id){
      $this->db->where('kode', $id);
      $this->db->delete('unit');
      return $this->db->affected_rows();
    }

  }
?>
