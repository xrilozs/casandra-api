<?php
  class Variasi1_model extends CI_Model{
    public $warna;

    function get_variasi1($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          warna LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('variasi1');
      return $query->result();
    }

    function count_variasi1($search=null){
      if($search){
        $where_search = "(
          warna LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->from('variasi1');
      return $this->db->count_all_results();
    }

    function get_variasi1_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('variasi1');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_variasi1($data){
      $this->warna      = $data['warna'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('variasi1', $this);
      return $this->db->affected_rows();
    }

    function update_variasi1($data){
      $this->warna = $data['warna'];

      $this->db->update('variasi1', $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_variasi1($id){
      $this->db->where('id', $id);
      $this->db->delete('variasi1');
      return $this->db->affected_rows();
    }

  }
?>
