<?php
  class Jenis_transaksi_model extends CI_Model{
    public $kode;
    public $nama;

    function get_jenis_transaksi($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          nama LIKE '%$search%' OR
          kode LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('jenis_transaksi');
      return $query->result();
    }

    function count_jenis_transaksi($search=null){
      if($search){
        $where_search = "(
          nama LIKE '%$search%' OR
          kode LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->from('jenis_transaksi');
      return $this->db->count_all_results();
    }

    function get_jenis_transaksi_by_id($id){
      $this->db->where("id", $id);
      $query = $this->db->get('jenis_transaksi');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_jenis_transaksi($data){
      $this->nama       = $data['nama'];
      $this->kode     = $data['kode'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('jenis_transaksi', $this);
      return $this->db->affected_rows();
    }

    function update_jenis_transaksi($data){
      $this->nama       = $data['nama'];
      $this->kode     = $data['kode'];

      $this->db->update('jenis_transaksi', $this, array('id' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_jenis_transaksi($id){
      $this->db->where('id', $id);
      $this->db->delete('jenis_transaksi');
      return $this->db->affected_rows();
    }

  }
?>
