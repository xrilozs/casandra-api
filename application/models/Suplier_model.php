<?php
  class Suplier_model extends CI_Model{
    public $nama;
    public $no_telpon;
    public $email;
    public $keterangan;

    function get_suplier($search=null, $order=null, $limit=null){
      if($search){
        $where_search = "(
          nama LIKE '%$search%' OR
          no_telpon LIKE '%$search%' OR
          email LIKE '%$search%' OR
          keterangan LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('suplier');
      return $query->result();
    }

    function count_suplier($search=null){
      if($search){
        $where_search = "(
          nama LIKE '%$search%' OR
          no_telpon LIKE '%$search%' OR
          email LIKE '%$search%' OR
          keterangan LIKE '%$search%'
        )";
        $this->db->where($where_search);
      }
      $this->db->from('suplier');
      return $this->db->count_all_results();
    }

    function get_suplier_by_id($id){
      $this->db->where("id_suplier", $id);
      $query = $this->db->get('suplier');
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_suplier($data){
      $this->nama       = $data['nama'];
      $this->no_telpon  = $data['no_telpon'];
      $this->email      = $data['email'];
      $this->keterangan = array_key_exists("keterangan", $data) ? $data['keterangan'] : null;
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('suplier', $this);
      return $this->db->affected_rows();
    }

    function update_suplier($data){
      $this->nama       = $data['nama'];
      $this->no_telpon  = $data['no_telpon'];
      $this->email      = $data['email'];
      $this->keterangan = array_key_exists("keterangan", $data) ? $data['keterangan'] : null;

      $this->db->update('suplier', $this, array('id_suplier' => $data['id']));
      return $this->db->affected_rows();
    }

    function delete_suplier($id){
      $this->db->where('id_suplier', $id);
      $this->db->delete('suplier');
      return $this->db->affected_rows();
    }

  }
?>
