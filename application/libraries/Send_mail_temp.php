<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;
require APPPATH.'libraries/phpmailer/src/Exception.php';
require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
require APPPATH.'libraries/phpmailer/src/SMTP.php';
class Send_mail{
    public function __construct() { 
        // parent::__construct(); 
    }

    function send($email, $subject, $message, $attachment=null, $filename=null){
        // require APPPATH.'libraries/phpmailer/src/Exception.php';
        // require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
        // require APPPATH.'libraries/phpmailer/src/SMTP.php';
        // PHPMailer object
        $response = false;
        $mail = new PHPMailer();
      
       // SMTP configuration
       $mail->isSMTP();
       $mail->Host     = SMTP_HOST; //sesuaikan sesuai nama domain hosting/server yang digunakan
       $mail->SMTPAuth = true;
       $mail->Username = SMTP_USERNAME; // user email
       $mail->Password = SMTP_PASSWORD; // password email
       $mail->SMTPSecure = 'ssl';
    //    $mail->Port     = 465;
       $mail->Port     = 587;

    //    $senderEmail = $isSender ? $email : COMPANY_NOREPLY_EMAIL;
       $mail->setFrom(COMPANY_NOREPLY_EMAIL, 'Postamu'); // user email
       $mail->AddEmbeddedImage('https://api.postamu.com/assets/web/logo.png', 'image_cid', 'logo.png');

       // Add a recipient
       $mail->addAddress($email); //email tujuan pengiriman email

       // Email subject
       $mail->Subject = $subject; //subject email

       // Set email format to HTML
       $mail->isHTML(true);

       // Email body content
       $mailContent = $message; // isi email
       $mail->Body = $mailContent;
       if($attachment && $filename) $mail->addStringAttachment($attachment, $filename);

       // Send email
       if(!$mail->send()){
          return 'Mailer Error: ' . $mail->ErrorInfo;
       }else{
           return 'Message has been sent';
       }
    }
}
?>