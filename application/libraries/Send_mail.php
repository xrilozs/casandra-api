<?php
require 'vendor/autoload.php'; // If you used Composer, otherwise adjust the path to PHPMailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require APPPATH.'libraries/phpmailer/src/Exception.php';
require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
require APPPATH.'libraries/phpmailer/src/SMTP.php';

class Send_mail{
  public function __construct() { 
      // parent::__construct(); 
  }
  function send($email, $subject, $message, $attachment=null, $filename=null){
    // Create a new PHPMailer instance
    $mail = new PHPMailer(true); // Passing `true` enables exceptions

    try {
        // SMTP configuration
        $mail->isSMTP();
        $mail->Host = SMTP_HOST;
        $mail->Port = 587;
        $mail->SMTPAuth = true;
        $mail->Username = SMTP_USERNAME;
        $mail->Password = SMTP_PASSWORD;

        // Sender and recipient settings
        $mail->setFrom(COMPANY_NOREPLY_EMAIL, 'Postamu');
        $mail->addAddress($email); // Replace with recipient's email and name

        // Email content
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $message;
        if($attachment && $filename) $mail->addStringAttachment($attachment, $filename);

        // Send the email
        $mail->send();
        return 'Email sent successfully';
    } catch (Exception $e) {
        return "Error: {$mail->ErrorInfo}";
    }
  }
}